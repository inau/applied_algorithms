//
// Created by ivan_ on 01-Oct-17.
//
#ifndef APPLIED_ALGORITHMS_CPP_PRIM_H
#define APPLIED_ALGORITHMS_CPP_PRIM_H

#include "IndexMinPQ.h"
#include "boost/heap/binomial_heap.hpp"
#include "Graph.h"
#include "mRand.h"

#include <queue>

using namespace mGraph;
using namespace boost::heap;
using namespace boost::heap::detail;

namespace Prim {

    struct compareInt {
        bool operator()(const int lhs, const int rhs) const {
            return lhs < rhs;
        }
    };

    class Eager {
    private:
        Graph *g;
        bool *inside;
        binomial_heap<int, compare< compareInt >> pq;
        binomial_heap<int, compare< compareInt >>::handle_type *handles;
        //algs4::IndexMinPQ<int, std::greater<int>> *pq; //weight
        int *dist;

        int other(Edge e, int current) {
            int oth = e.from != current ? e.from : e.to;
            return oth;
        }

        void enqueue(Vertex v) {
            this->inside[ v.identifier() ] = true;
            Edge e; int oth;
            for(int i  = 0; i < v.adjacent().size(); i++) {
                e = g->getEdges()[ v.adjacent()[i] ];
                oth = other(e, v.identifier());

                if( inside[oth] ) continue;
                if( e.weight < dist[oth] ) {
                    dist[oth] = e.weight;
                    pq.update(handles[oth], dist[oth]);
                }
            }
        }

        std::vector<int> mst() {
            std::vector<int> mst;
            for (int i = 0; i < g->getEdges().size(); i++) {
                if( dist[i] < INT32_MAX ) mst.push_back( dist[i] );
            }
            return mst;
        }

    public:
        Eager(Graph *g) {
            this->g = g;
            this->inside = new bool[g->size()];
            std::fill_n(inside, g->size(), false);

            this->dist = new int[g->size()];
            std::fill_n(dist, g->size(), INT32_MAX);

            this->handles = new binomial_heap<int, compare<compareInt>>::handle_type[g->size()];
        }

        long compute() {
            //v0
            dist[0] = 0;

            //insert all
            for (int j = 0; j < g->size(); j++) {
                handles[j] = pq.push(dist[j]);
            }

            int v; int vptr = 0;
            while( !pq.empty() ) {
                v = pq.top();
                std::cerr << "item " << v ;
                pq.pop();

                enqueue( g->getVertex(v) );

                while( pq.empty() ) { //min spanning forest
                    if(vptr == g->size()) break; //all vertices have been traversed
                    if( inside[vptr] ) vptr++; //vertex has been visited
                    else enqueue(g->getVertex(vptr)); //new tree at v
                }
            }

            std::vector<int> mm = mst();
            return mRand::mstToInt( mm );
        }
    };

    //Needed for priority queue - Default is max prio so switched to greater than
    class EdgeCompareWeight {
    public:
        bool operator()(const Edge lhs, const Edge rhs) {
            return (lhs.weight >= rhs.weight);
        }
    };

    class Lazy {
    protected:
        mGraph::Graph *g;
        void EnqueueEdgesFromVertex(Vertex v, std::priority_queue<Edge, std::vector<Edge>,EdgeCompareWeight> *pq, bool *visited) {
            //std::cerr << "updating visited structure for " << v.identifier() << std::endl;
            visited[ v.identifier() ] = true;
            //std::cerr << "updated visited structure " << std::endl;
            std::vector<int> edges = v.adjacent();

            Edge e;
            for(int i  = 0; i < edges.size(); i++) {
                e = g->getEdges()[i];
                //only enqueue edges where one vertex hasn't been visited
                if(!visited[e.from] || !visited[e.to] ) {
                    //std::cerr << "Pushing element " << i << " (" << e.from << ", " << e.to << ")" << std::endl;
                    pq->push( e );
                }
            }
        };

    public:
        Lazy(mGraph::Graph *g) {
            this->g = g;
        }

        long compute(){
            std::vector<Edge> mst;
            long mstsize = 0;
            //std::cerr << "Created MST" << std::endl;

            bool *inside = new bool[g->size()];
            std::fill_n(inside, g->size(), false); //undefined behaviour at init according to the web - so i fill explicitly
            //std::cerr << "Created Visited structure" << std::endl;

            std::priority_queue<Edge, std::vector<Edge>, EdgeCompareWeight> fringe;
            //std::cerr << "Created fringe" << std::endl;

            //Enqueue V0
            inside[0] = true;
            Vertex v = g->getVertex(0);
            //std::cerr << "Found vertex " << v.identifier() << std::endl;

            EnqueueEdgesFromVertex(v, &fringe, inside);
            //std::cerr << "Enqueued v0" << std::endl;

            Edge e; int v1, v2, vptr = 0;
            while( !fringe.empty() ) {
                e = fringe.top(); //get next edge
                fringe.pop(); //dequeue it
                v1 = e.from;
                v2 = e.to;

                //make sure that edge vertices havent been added
                if( inside[v1] && inside[v2] ) continue;

                //grow mst by edge
                mst.push_back(e);

                //Check if v1 end is outside
                if( !inside[v1] ) {
                    EnqueueEdgesFromVertex(g->getVertex(e.from), &fringe, inside);
                }
                //check if v2 end is outside
                if( !inside[v2] ) {
                    EnqueueEdgesFromVertex(g->getVertex(e.to), &fringe, inside);
                }

                while( fringe.empty() ) { //min spanning forest
                    if(vptr == g->size()) break;
                    if( inside[vptr++] ) continue;
                    else EnqueueEdgesFromVertex(g->getVertex(vptr), &fringe, inside );
                }
            }
            return mRand::mstToInt(mst);
        };
    };

}
#endif