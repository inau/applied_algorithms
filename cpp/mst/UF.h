//
// Created by Ivan on 04-Oct-17.
//

#ifndef APPLIED_ALGORITHMS_CPP_UF_H
#define APPLIED_ALGORITHMS_CPP_UF_H

#include <algorithm>

namespace UF {

class UF {
    protected:
        int *rank;
        int *parent;

        bool isRoot(int i) {
            return parent[i] == i;
        }

        int findRoot(int i) {
            int t = i;
            while( !isRoot(t) ) {
                t = parent[t];
            }

            //compress
            parent[i] = t;

            return t;
        }
        int N;

    public:
        UF(int V) {
            N = V;
            rank = new int[V];
            std::fill_n(rank, V, 1);
            parent = new int[V];
            for (int i = 0; i < V; ++i) {
                parent[i] = i;
            }
        }

        void unite(const int u, const int v) {
            //std::cerr << "++ unite " << u << ", " << v << std::endl;
            int r;
            if( rank[u] >= rank[v] ) {
                r = findRoot(u);
                parent[v] = r;
                rank[r] += rank[v];
            }
            else {
                r = findRoot(v);
                parent[u] = r;
                rank[r] += rank[u];
            }
        }

        int find(const int u) {
            return findRoot(u);
        }

        void print() {
            std::cerr << "index:  ";
            for (int i = 0; i < N; i++) {
                std::cerr << i << " ";
            }
            std::cerr << std::endl << "Parent: ";
            for (int i = 0; i < N; i++) {
                std::cerr << parent[i] << " ";
            }
            std::cerr << std::endl << "Rank:   ";
            for (int i = 0; i < N; i++) {
                std::cerr << rank[i] << " ";
            }
            std::cerr << std::endl;
        }

};

}

#endif //APPLIED_ALGORITHMS_CPP_UF_H
