//
// Created by Ivan on 04-Oct-17.
//

#ifndef APPLIED_ALGORITHMS_CPP_KRUSKAL_H
#define APPLIED_ALGORITHMS_CPP_KRUSKAL_H

#include <vector>
#include <algorithm>
#include "Graph.h"
#include "UF.h"

namespace Kruskal {

    //Needed for priority queue - Default is max prio so switched to greater than
    class EdgeCompareWeight {
    public:
        bool operator()(const mGraph::Edge lhs, const mGraph::Edge rhs) {
            return (lhs.weight < rhs.weight);
        }
    };

    std::vector<Edge> mst(mGraph::Graph *g) {
        std::vector<Edge> mst;

        //sort edges by weight
        std::vector<mGraph::Edge> sortedEdges = g->getEdges();
        std::sort(sortedEdges.begin(), sortedEdges.end(), EdgeCompareWeight() );

        //UnionFind structure
        UF::UF unionfind( g->size() );
        //add smallest edge
        unionfind.unite( sortedEdges[0].from, sortedEdges[0].to );

        mGraph::Edge e;
        for (int i = 1; i < sortedEdges.size(); i++) {
            e = sortedEdges[i];

            //find parent affiliation
            int pi = unionfind.find( e.from );
            int pu = unionfind.find(e.to);

            //different components
            if( pi != pu ) {
                unionfind.unite(e.from, e.to);
                mst.push_back(e);
            }

            //if edges added reach V-1 we have a edge for every pair of nodes
            if( !(mst.size() < g->size()) ) break;
        }
        return mst;
    }

    long compute(mGraph::Graph *g) {
        std::vector<Edge> m = mst(g);
        return mRand::mstToInt( m );
    }

}

#endif //APPLIED_ALGORITHMS_CPP_KRUSKAL_H
