//
// Created by Ivan on 26-Sep-17.
//

#ifndef APPLIED_ALGORITHMS_CPP_GRAPH_H
#define APPLIED_ALGORITHMS_CPP_GRAPH_H

#include <iostream>
#include <vector>

namespace mGraph {
/*******************************************************
 ***** Edge
 *******************************************************/
    struct Edge {
        int from;
        int to;
        int weight;
    };

/********************************************************
 ***** Vertex
 ********************************************************/
    class Vertex {
        private:
            int id;
            std::vector<int> adj;
        public:
            Vertex(){};
            Vertex(int i) { id = i; };

            void addEdge(int i) {
                adj.push_back(i);
            }

            std::vector<int> adjacent() {
                return adj;
            }

            int identifier() {
                return id;
            }
    };

/********************************************************
 ***** Graph
 ********************************************************
 * 0 indexed
 ********************************************************/
    class Graph {
        private:
            Vertex *vertices;
            std::vector<Edge> edges;
            int N;
            bool vr;
        public:
            Graph(int vSize, bool VReq = true) {
                vr = VReq;
                if(vr) {
                    vertices = new Vertex[vSize];
                    std::cerr << "Created graph ";

                    for (int i = 0; i < vSize; i++) {
                        vertices[i] = Vertex(i);
                    }
                    std::cerr << "with " << vSize << " vertices" << std::endl;
                }
                N = vSize;
            }

            int size() {
                return N;
            }

            void addEdge(int const i, int const j, int w = 1) {
                Edge e = {i,j,w};
                edges.push_back(e);
                //std::cerr << "adding (" << e.from << ", " << e.to << ", " << e.weight << ")" << std::endl;
                if(vr) {
                    vertices[i].addEdge(edges.size()-1);
                    vertices[j].addEdge(edges.size()-1);
                }
            }

            Vertex getVertex(int const i) {
                if(vr) return vertices[i];
                else return NULL;
            }

            std::vector<Edge> getEdges() {
                return edges;
            }
    };

}
#endif //APPLIED_ALGORITHMS_CPP_GRAPH_H
