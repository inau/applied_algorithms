//
// Created by ivan_ on 01-Oct-17.
//

/**
   * Unit tests the {@code IndexMinPQ} data type.
   *
   * @param args the command-line arguments
   */
#include "IndexMinPQ.h"

using namespace algs4;


static void unit_test() {
    // insert a bunch of strings
    std::string strings[] = { "it", "was", "the", "best", "of", "times", "it", "was", "the", "worst" };

    IndexMinPQ<std::string> *pq = new IndexMinPQ<std::string>(10);
    for (int i = 0; i < 10; i++) {
        pq->insert(i, strings[i]);
    }

    // delete and print each key
    while (!pq->isEmpty()) {
        int i = pq->delMin();
        std::cout << i << " " << strings[i] << std::endl;
    }

    // reinsert the same strings
    for (int i = 0; i < 10; i++) {
        pq->insert(i, strings[i]);
    }

    while (!pq->isEmpty()) {
        pq->delMin();
    }

}

int main() {
    unit_test();
}