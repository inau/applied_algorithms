/******************************************************************************
 *
 *  Minimum-oriented indexed PQ implementation using a binary heap.
 *
 ******************************************************************************/
/**
 *  The {@code IndexMinPQ} class represents an indexed priority queue of generic keys.
 *  It supports the usual <em>insert</em> and <em>delete-the-minimum</em>
 *  operations, along with <em>delete</em> and <em>change-the-key</em>
 *  methods. In order to let the client refer to keys on the priority queue,
 *  an integer between {@code 0} and {@code maxN - 1}
 *  is associated with each key—the client uses this integer to specify
 *  which key to delete or change.
 *  It also supports methods for peeking at the minimum key,
 *  testing if the priority queue is empty, and iterating through
 *  the keys.
 *  <p>
 *  This implementation uses a binary heap along with an array to associate
 *  keys with integers in the given range.
 *  The <em>insert</em>, <em>delete-the-minimum</em>, <em>delete</em>,
 *  <em>change-key</em>, <em>decrease-key</em>, and <em>increase-key</em>
 *  operations take logarithmic time.
 *  The <em>is-empty</em>, <em>size</em>, <em>min-index</em>, <em>min-key</em>,
 *  and <em>key-of</em> operations take constant time.
 *  Construction takes time proportional to the specified capacity.
 *  <p>
 *  For additional documentation, see <a href="http://algs4.cs.princeton.edu/24pq">Section 2.4</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 *
 *  @param <Key> the generic type of key on this priority queue
 *
 *  Cpp port doing refactorings to get similar behaviour
 *
 */

#ifndef APPLIED_ALGORITHMS_CPP_ALGS_INDEXMINPQ_H
#define APPLIED_ALGORITHMS_CPP_ALGS_INDEXMINPQ_H

#include <algorithm>
#include <iostream>

using namespace std;

namespace algs4 {

    template<class T, class Comparator = std::greater<T> >
    class IndexMinPQ {
    private:
        int maxN;        // maximum number of elements on PQ
        int n;           // number of elements on PQ
        int *pq;        // binary heap using 1-based indexing
        int *qp;        // inverse of pq - qp[pq[i]] = pq[qp[i]] = i
        T *keys;      // keys[i] = priority of i
        Comparator cmp = Comparator();

        bool greater(int i, int j) {
            return compareTo(keys[pq[i]], keys[pq[j]]);
        }

        void exch(int i, int j) {
            int swap = pq[i];
            pq[i] = pq[j];
            pq[j] = swap;
            qp[pq[i]] = i;
            qp[pq[j]] = j;
        }

        void swim(int k) {
            while (k > 1 && greater(k/2, k)) {
                exch(k, k/2);
                k = k/2;
            }
        }

        void sink(int k) {
            while (2*k <= n) {
                int j = 2*k;
                if (j < n && greater(j, j+1)) j++;
                if (!greater(k, j)) break;
                exch(k, j);
                k = j;
            }
        }


        int compareTo(T a, T b) { return cmp(a, b); };

    public:
        /**
       * Initializes an empty indexed priority queue with indices between {@code 0}
       * and {@code maxN - 1}.
       * @param  maxN the keys on this priority queue are index from {@code 0}
       *         {@code maxN - 1}
       * @throws IllegalArgumentException if {@code maxN < 0}
       */
        IndexMinPQ(int maxN) {
            if (maxN < 0) std::cerr << "IndexMinPQ can not be instantiated with size of " << maxN << std::endl;
            this->maxN = maxN;
            n = 0;
            keys = new T[maxN + 1];    // make this of length maxN??
            pq   = new int[maxN + 1];
            qp   = new int[maxN + 1];                   // make this of length maxN??
            for (int i = 0; i <= maxN; i++)
                qp[i] = -1;
        }

        /**
 * Returns true if this priority queue is empty.
 *
 * @return {@code true} if this priority queue is empty;
 *         {@code false} otherwise
 */
        bool isEmpty() {
            return this->n == 0;
        }

        /**
 * Is {@code i} an index on this priority queue?
 *
 * @param  i an index
 * @return {@code true} if {@code i} is an index on this priority queue;
 *         {@code false} otherwise
 * @throws IllegalArgumentException unless {@code 0 <= i < maxN}
 */
        bool contains(int i) {
            if (i < 0 || i >= maxN) std::cerr << "IllegalArgument, index out of bounds" << std::endl;
            return qp[i] != -1;
        }

        /**
 * Returns the number of keys on this priority queue.
 *
 * @return the number of keys on this priority queue
 */
        int size() {
            return n;
        }

        /**
 * Associates key with index {@code i}.
 *
 * @param  i an index
 * @param  key the key to associate with index {@code i}
 * @throws IllegalArgumentException unless {@code 0 <= i < maxN}
 * @throws IllegalArgumentException if there already is an item associated
 *         with index {@code i}
 */
        void insert(int i, T key) {
            if (i < 0 || i >= maxN) std::cerr << "IllegalArgument, index out of bounds" << std::endl;
            if (contains(i)) std::cerr << "IllegalArgument, index already in use, use change function instead" << std::endl;
            n++;
            qp[i] = n;
            pq[n] = i;
            keys[i] = key;
            swim(n);
        }

        /**
 * Returns an index associated with a minimum key.
 *
 * @return an index associated with a minimum key
 * @throws NoSuchElementException if this priority queue is empty
 */
        int minIndex() {
            if (n == 0) {
                std::cerr << "QueueUnderflow, nothing in queue" << std::endl;
                return -1;
            }
            return pq[1];
        }

        /**
 * Returns a minimum key.
 *
 * @return a minimum key
 * @throws NoSuchElementException if this priority queue is empty
 */
        T minKey() {
            if (n == 0) {
                std::cerr << "QueueUnderflow, nothing in queue" << std::endl;
                return NULL;
            }
            return keys[pq[1]];
        }

        /**
 * Removes a minimum key and returns its associated index.
 * @return an index associated with a minimum key
 * @throws NoSuchElementException if this priority queue is empty
 */
        int delMin() {
            if (n == 0) {
                std::cerr << "QueueUnderflow, nothing in queue" << std::endl;
                return -1;
            }
            int min = pq[1];
            exch(1, n--);
            sink(1);
            //assert min == pq[n+1];
            qp[min] = -1;        // delete
            //keys[min] = NULL;    // to help with garbage collection
            pq[n+1] = -1;        // not needed
            return min;
        }

        /**
 * Returns the key associated with index {@code i}.
 *
 * @param  i the index of the key to return
 * @return the key associated with index {@code i}
 * @throws IllegalArgumentException unless {@code 0 <= i < maxN}
 * @throws NoSuchElementException no key is associated with index {@code i}
 */
        T keyOf(int i) {
            if (i < 0 || i >= maxN) {
                std::cerr << "IllegalArgument, index out of bounds" << std::endl;
                return NULL;
            }
            if (!contains(i)) {
                std::cerr << "IllegalArgument, index is not in the priority queue" << std::endl;
                return NULL;
            }
            else return keys[i];
        }

        /**
 * Change the key associated with index {@code i} to the specified value.
 *
 * @param  i the index of the key to change
 * @param  key change the key associated with index {@code i} to this key
 * @throws IllegalArgumentException unless {@code 0 <= i < maxN}
 * @throws NoSuchElementException no key is associated with index {@code i}
 */
        void changeKey(int i, T key) {
            if (i < 0 || i >= maxN) {
                std::cerr << "IllegalArgument, index out of bounds" << std::endl;
                return;
            }
            if (!contains(i)) {
                std::cerr << "IllegalArgument, index is not in the priority queue" << std::endl;
                return;
            }
            keys[i] = key;
            swim(qp[i]);
            sink(qp[i]);
        }

        /**
  * Decrease the key associated with index {@code i} to the specified value.
  *
  * @param  i the index of the key to decrease
  * @param  key decrease the key associated with index {@code i} to this key
  * @throws IllegalArgumentException unless {@code 0 <= i < maxN}
  * @throws IllegalArgumentException if {@code key >= keyOf(i)}
  * @throws NoSuchElementException no key is associated with index {@code i}
  */
        void decreaseKey(int i, T key) {
            if (i < 0 || i >= maxN) {
                std::cerr << "IllegalArgument, index out of bounds" << std::endl;
                return;
            }
            if (!contains(i)) {
                std::cerr << "IllegalArgument, index is not in the priority queue" << std::endl;
                return;
            }
            if ( compareTo(keys[i], key) <= 0) {
                std::cerr << "IllegalArgument, Calling decreaseKey() with given argument would not strictly decrease the key" << std::endl;
                return;
            }
            keys[i] = key;
            swim(qp[i]);
        }

        /**
 * Increase the key associated with index {@code i} to the specified value.
 *
 * @param  i the index of the key to increase
 * @param  key increase the key associated with index {@code i} to this key
 * @throws IllegalArgumentException unless {@code 0 <= i < maxN}
 * @throws IllegalArgumentException if {@code key <= keyOf(i)}
 * @throws NoSuchElementException no key is associated with index {@code i}
 */
        void increaseKey(int i, T key) {
            if (i < 0 || i >= maxN) {
                std::cerr << "IllegalArgument, index out of bounds" << std::endl;
                return;
            }
            if (!contains(i)) {
                std::cerr << "IllegalArgument, index is not in the priority queue" << std::endl;
                return;
            }
            if ( compareTo(keys[i], key) >= 0) {
                std::cerr << "IllegalArgument, Calling increaseKey() with given argument would not strictly increase the key" << std::endl;
                return;
            }
            keys[i] = key;
            sink(qp[i]);
        }

        /**
 * Remove the key associated with index {@code i}.
 *
 * @param  i the index of the key to remove
 * @throws IllegalArgumentException unless {@code 0 <= i < maxN}
 * @throws NoSuchElementException no key is associated with index {@code i}
 */
        void deleteEntry(int i) {
            if (i < 0 || i >= maxN) {
                std::cerr << "IllegalArgument, index out of bounds" << std::endl;
                return;
            }
            if (!contains(i)) {
                std::cerr << "IllegalArgument, index is not in the priority queue" << std::endl;
                return;
            }
            int index = qp[i];
            exch(index, n--);
            swim(index);
            sink(index);
            keys[i] = NULL;
            qp[i] = -1;
        }

    };


}
//Copyright © 2000–2017, Robert Sedgewick and Kevin Wayne.

//Last updated: Fri Jul 7 09:52:30 EDT 2017.
#endif //APPLIED_ALGORITHMS_CPP_ALGS_INDEXMINPQ_H