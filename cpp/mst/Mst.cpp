//
// Created by Ivan on 26-Sep-17.
//
#include <algorithm>
#include <queue>
#include <fstream>
#include "Graph.h"
#include "mRand.h"
#include "Prim.h"
#include "Kruskal.h"
#include "unistd.h"

using namespace mGraph;

/***********************************************************************************
 *  Helper functions for creating graphs of different types (complete, grid, file) *
 ***********************************************************************************/

Graph *gridGraph(int x, int y, int seed) {
    std::cerr << "Seed " << seed << ", X " << x << " Y " << y << std::endl;
    int v = x * y;
    Graph *g = new Graph( v );
    int *seeds = new int[ v ];
    mRand::genSeeds(seed, v, seeds);

        for (int i = 0; i < v; i++) {
            //right
            if(i % x != x-1) g->addEdge(i,i+1, mRand::getEdgeWeight(i,i+1,seeds));
            //below
            if( i < (v - y) ) g->addEdge(i,i+x, mRand::getEdgeWeight(i,i+x,seeds));
        }
    std::cerr << "Graph initialised" << std::endl;
    return g;
};

Graph *denseGraph(int v, int seed) {
    std::cerr << "Seed " << seed << ", V " << v << std::endl;
    Graph *g = new Graph( v );
    int *seeds = new int[ v ];
    mRand::genSeeds(seed, v, seeds);

    for (int i = 0; i < v; i++) {
        for (int j = i+1; j < v; j++) {
            if(i == j) continue;
            else g->addEdge(i,j, mRand::getEdgeWeight(i,j,seeds));
        }
    }
    std::cerr << "Graph initialised" << std::endl;
    return g;
};

Graph *fileGraph(int v, int seed, std::string source) {
    std::cerr << "Seed " << seed << ", V " << v << std::endl;
    Graph *g = new Graph( v );
    int *seeds = new int[ v ];
    mRand::genSeeds(seed, v, seeds);

    std::fstream in(source);
    std::string line;
    std::getline(in, line);
    while( line[0] == '#' ) std::getline(in, line);
    int x,y;
    while( in ) {
        in >> x;
        in >> y;
        g->addEdge(x,y, mRand::getEdgeWeight(x,y,seeds));
    }
    std::cerr << "reached EoF" << std::endl;
    std::cerr << "Graph initialised" << std::endl;
    return g;
};

/*****************************************************************
 * Handles interpretation of input
 ****************************************************************/
Graph *handleCases(int c, char** argv) {
    switch (c) {
        case 3:
            std::cerr << "Connected: ";
            return denseGraph(std::atoi( argv[2] ), std::atoi( argv[1] ) );
        case 4:
            std::cerr << "Grid: ";
            return gridGraph(std::atoi( argv[2] ), std::atoi( argv[3] ), std::atoi( argv[1] ) );
        case 5:
            std::cerr << "File: ";
            return fileGraph(std::atoi( argv[3] ),std::atoi( argv[1] ), argv[2]);
        default:
            std::cerr << "unable to decode arguments" << std::endl;
            return NULL;
    }
};

int main(int c, char** argv) {
    Graph *g = handleCases(c, argv);
    if( !g ) exit(-1);
    std::cerr << "V " << g->size() << " E " << g->getEdges().size() << std::endl;


    int kr = Kruskal::compute(g);
    std::cout << "Kruskals " << kr << std::endl;
    int pr = Prim::Lazy(g).compute();
    std::cout << "PrimLazy " << pr << std::endl;
    int pre = Prim::Eager(g).compute();
    std::cout << "PrimEagr " << pre << std::endl;

};