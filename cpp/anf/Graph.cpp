//
// Created by Ivan on 11-Sep-17.
//

#include "Graph.h"

// rows of v-u input (integer id's)
// each edge represented once (undirected)

int main() {
    Graph g = Graph();

    std::cerr << "before " << g.getVertices().size() << std::endl;
    g.addEdge(1,2);
    g.addEdge(1,3);
    g.addEdge(2,3);
    g.addEdge(3,4);
    std::cerr << "after " << g.getVertices().size() << std::endl;

    //BreathFirstSearch bfs = BreathFirstSearch(g);
    for (int i = 1; i <= g.getVertices().size(); i++) {
        //bfs.BFS(i);
        std::cerr << "++ Done with BFS of " << i << std::endl;
    }
//    bfs.printDistances();
}
