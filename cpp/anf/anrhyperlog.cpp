//
// Created by Ivan on 19-Sep-17.
//

#include "Graph.h"
#include "Approx.h"

class anrGraph : public Graph {
protected:

public:
    int Estimate() {
        map<int, ApproxSet> m;
        map<int, ApproxSet> c;

        int v;

        for(auto entry : Vertices) {
            v = entry.first;
            c[v].add(v);
        }

        int d = 0;
        double limit = (std::pow(Vertices.size(),2))/2;
        double reach;
        while( true ) {
            reach = 0.0;
            for(auto entry : Vertices) {
                v = entry.first;
                m[v].addSet(  &c[v] );

                for(auto w : Vertices[v]) {
                    m[v].addSet( &c[w] );
                }
                reach += m[v].sizeEstimate();
            }

            for( auto vv : Vertices ) {
                v = vv.first;
                c[v] = m[v];
            }
            d++;
            if( reach >= limit ) break;
        }
        return d;
    }

};

int main() {
    anrGraph ag = anrGraph();

    int x,y;
    while( std::cin >> x ) {
        std::cin >> y;
        ag.addEdge(x,y);
    }

    std::cout << ag.Estimate() << std::endl;
}