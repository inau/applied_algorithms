//
// Created by Ivan on 11-Sep-17.
//
#include <unordered_set>
#include <map>
#include <queue>
#include <iostream>

#ifndef APPLIED_ALGORITHMS_CPP_GRAPH_H
#define APPLIED_ALGORITHMS_CPP_GRAPH_H

using std::unordered_set;
using std::map;
using std::queue;

class Graph {
    public:
        void addEdge(int v, int u) {
            Vertices[v].insert(u);
        }
    map<int,unordered_set<int>> getVertices() {
        return Vertices;
    };
    protected:
        map<int,unordered_set<int>> Vertices;
};

/**
class BreathFirstSearch {
public:
    BreathFirstSearch(Graph g) {
        this->G = g;
        this->D = new int[g.getVertices().size()] {0};
    }

    void BFS(int i) {
        std::queue<int> queue;
        std::unordered_set<int> visited;

        visited.insert(i);
        queue.push(i);

        int d = 0;
        while (!queue.empty()) {
            int nodes = queue.size();
            D[d++] += nodes;
            for (int j = 0; j < nodes; j++) {
                int v = queue.front();
                std::cerr << "Iterating: " << v << " ";
                queue.pop();
                std::unordered_set<int>::iterator edges = G.getEntry(v).second.edges().begin();
                int u;
                std::cerr << " connects -> ";
                while ( u = edges._M_cur->_M_v() ) {
                    std::cerr << " " << u ;
                    if (!visited.count(u) > 0 ) {
                        std::cerr << " push " << u << ",";
                        visited.insert(u);
                        queue.push(u);
                    }
                    if( !edges._M_cur->_M_nxt ) break;
                    else edges._M_incr();
                }
                std::cerr << std::endl << "finished: " << v << std::endl;
            }
        }
    }

    void printDistances() {
        for (int i = 0; i < G.getVertices().size(); i++) {
            if (D[i] > 0) std::cout << "D[" << i << "] " << D[i] << std::endl;
        }
    }

protected:
    Graph G;
    int *D;
};
**/
#endif //APPLIED_ALGORITHMS_CPP_GRAPH_H
