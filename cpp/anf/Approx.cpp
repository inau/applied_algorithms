#include "Approx.h"

int main() {
    int i;
    while(std::cin.peek() == '#') {
        std::string s;
        std::getline( std::cin, s  );
    }
    ApproxSet sets[2];
    int sid = 0;
    while( sid < 2 ) {
        sets[sid] = ApproxSet();
        std::string line;
        std::getline(std::cin, line);
        mread(&line, &sets[sid] );
        sid++;
    }
    double before = sets[0].sizeEstimate(), beforeb = sets[1].sizeEstimate();
    std::cerr <<  "#Read sets A: " << before << " B: "<< beforeb << std::endl;
    sets[0].addSet(&sets[1]);
    double after = sets[0].sizeEstimate();
    std::cerr << "#SetUnion: " << after << std::endl;
    double ratio = ((before + beforeb) - after) / before;
    std::cerr <<  "#Ratio " << ratio << std::endl;
    std::cerr <<  "#Ended " << std::endl;
    std::string ss = ratio >= 0.8? "same" : "disjoint";
    std::cout << "almost " << ss << std::endl;
}
