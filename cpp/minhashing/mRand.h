//
// Created by Ivan on 26-Sep-17.
//

#ifndef APPLIED_ALGORITHMS_CPP_MRAND_H
#define APPLIED_ALGORITHMS_CPP_MRAND_H
#include <vector>

namespace mRand {

    int xorshift32(int seed) {
        int ret = seed;
        ret ^= ret << 13;
        ret ^= ret >> 17;
        ret ^= ret << 5;
        return ret;
    }

    void genSeeds(int seed, int numV, int *vertex_seeds) {
        vertex_seeds[0] = mRand::xorshift32(seed);
        for( int i = 1; i < numV; i++) {
            vertex_seeds[i] = mRand::xorshift32(vertex_seeds[i-1] ^seed);
        }
    }

    int getEdgeWeight(int v, int u, int *seeds, int MOD = 100000) {
        return mRand::xorshift32( seeds[v] ^ seeds[u] ) % MOD;
    }

    int hashRand(int inIndex){
        const static int b = 0x5f375a86; //bunch of random bits
        for(int i = 0; i < 8; i++)
        {
            inIndex = (inIndex + 1)*( (inIndex >> 1)^b);
        }
        return inIndex;
    }

}

#endif //APPLIED_ALGORITHMS_CPP_MRAND_H
