#ifndef MINHASH_HASH_H
#define MINHASH_HASH_H


#include "mRand.h"
#include <queue>
#include <iostream>

namespace Hash {
    // Threshold for buckets
    int THRESHOLD = 1500;

    typedef long long int int64;

    class pairCompare {
    public:
        bool operator() (const std::pair<int,int> i1, const std::pair<int,int> i2) {
            return i1.second < i2.second;
        }
    };

    /***
     *      block index
     *  right shift 6 gange (svarende til at bit 64,128 er de første) ie. 7,8
     *
     *      local index = mask 63 for at få de første 6 bits
     ***/
    struct vector256 {
        int vid;
        const short size = 4;
        int64 blocks[4];

        void print() {
            std::cerr << blocks[0] << " " << blocks[1] << " " << blocks[2] << " " << blocks[3] << std::endl;
        }

        bool isEqual( vector256 other) {
            bool v = true;
            for (int i = 0; i < size; i++) {
                v = v && other.blocks[i] == this->blocks[i];
            }
            return v;
        }

        bool is_set_local(int lid, int bid) {
            return (blocks[bid] & (1 << lid)) != 0;
        }

        bool is_set(int bit) {
             int bid = bit >> 6;
             int lid = bit & 63;
             return is_set_local(lid, bid);
            /**
            if( bit < 128 ) {
                if( bit < 64) {
                    return is_set_local( bit, 0 );
                }
                return is_set_local( bit - 64, 1 );
            }
            else {
                if(bit < 192) {
                    return is_set_local(bit - 128, 2);
                }
                return is_set_local(bit - 192, 3);
            }
             **/
        }
        vector256(int vid, int64 i1, int64 i2, int64 i3, int64 i4) {
            this->vid = vid;
            blocks[0]=i1;
            blocks[1]=i2;
            blocks[2]=i3;
            blocks[3]=i4;
        }
    };

    struct Correlation {
        int vid1, vid2;
        int sim;
        double jac;

        bool isBest( int other_corr ) {
            return this->sim > other_corr;
        }

        void print() {
            if( vid1 < vid2 ) {
                std::cout << vid1 << " " << vid2;
            }
            else                 std::cout << vid2 << " " << vid1;
            std::cout << std::endl;
        }

        Correlation(int v1, int v2, int sim) : vid1(v1), vid2(v2), sim(sim) {};
    };


    class Hash {
    protected:
        int size;
        int* indexes;

    public:
        Hash() {};
        Hash(int sz, int seed) {
            //std::cerr << "hash constr called" <<std::endl;
            this->size = sz;
            int seeds[sz];
            mRand::genSeeds(seed, sz, seeds);
            indexes = new int[sz];

            std::priority_queue< std::pair<int,int>, std::vector< std::pair<int,int>>, pairCompare> q;

            std::pair<int,int> p;
            for (int i = 0; i < sz; i++) {
                p = std::make_pair( i, seeds[i] );
                q.push( p );
            }

            int i = 0;
            while ( !q.empty() ) {
                p = q.top();
                indexes[i++] = p.first;
                q.pop();
            }

        }

        const int block_bits = 64;
        int hashVector(vector256 v, int block) {
            int offset = block_bits * block;
            if(offset < 0) offset = 0;

            for (int i = offset; i < size; i++) {
                if( v.is_set( indexes[i] ) ) return indexes[i];
            }
            //std::cerr << "no set bits" << std::endl;
            return -1;
        }

        int hashVector(vector256 v) {
            for (int i = 0; i < size; i++) {
               if( v.is_set( indexes[i] ) ) return indexes[i];
            }
            //std::cerr << "no set bits" << std::endl;
            return -1;
        }

        void printIndexes() {
            for (int i = 0; i < size; i++) {
                std::cerr << indexes[i] << " ";
            }
            std::cerr << std::endl;
        }

    };

    //jacard
    double jac_similarity(vector256 v1, vector256 v2, int corr) {
        int64 tmp; int not_common = 0;
        for (int i = 0; i < 4; i++) {
            tmp = v1.blocks[i] ^ v2.blocks[i];
            not_common += __builtin_popcountll(tmp);
        }
        return corr / (not_common + corr);
    }

    //returns number of matching set bits
    int correlation(vector256 v1, vector256 v2, int block = 4) {
        int64 tmp;
        int setb = 0;

        switch(block) {
            case 4:
                setb += correlation(v1,v2,0) + correlation(v1,v2,1) + correlation(v1,v2,2) + correlation(v1,v2,3);
                return setb;
            case 3:
                tmp = v1.blocks[block] & v2.blocks[block]; // AND to get 1-1 pairs
                return __builtin_popcountll(tmp); // bits set present in both
            case 2:
                tmp = v1.blocks[block] & v2.blocks[block]; // AND to get 1-1 pairs
                return __builtin_popcountll(tmp); // bits set present in both
            case 1:
                tmp = v1.blocks[block] & v2.blocks[block]; // AND to get 1-1 pairs
                return __builtin_popcountll(tmp); // bits set present in both
            case 0:
                tmp = v1.blocks[block] & v2.blocks[block]; // AND to get 1-1 pairs
                return __builtin_popcountll(tmp); // bits set present in both
            default:
                return 0;
        }
    }

    struct bucket {
        std::vector<vector256> *items = new std::vector<vector256>();

        bool isFull() {
            return items->size() > THRESHOLD;
        }
        void insert(vector256 v) {
            items->push_back(v);
        }
    };

}

#endif