#include "Hash.h"
#include <map>
#include <fstream>
#include <chrono>

Hash::bucket findOrCreateBucket(const int hash, std::map<int, Hash::bucket> &sorted) {
    if( sorted.find( hash ) == sorted.end() ) {
        sorted.insert( std::pair<int, Hash::bucket>(hash, Hash::bucket() ) );
    }
    return sorted.find( hash ).operator->()->second;
}

std::vector<Hash::Hash*> hashes;

const int MOD = 64;

void splits(Hash::bucket &full_bucket, std::vector<Hash::bucket> &dist, const int level) {
    if( level == hashes.size() ) hashes.push_back( new Hash::Hash(256, level * 64 ) );

    std::map<int, Hash::bucket> sorted;
    //std::cerr << "lvl " << level << ", sz " << full_bucket.items->size() << std::endl;

    int h;
    Hash::bucket b;
    const int local_mod = MOD / 2^level;
    for(int i = 0; i < full_bucket.items->size(); i++) {
        h = hashes[level]->hashVector( full_bucket.items->at(i) );
        b = findOrCreateBucket( h % local_mod / 2^level, sorted );
        b.insert( full_bucket.items->at(i) );
    }

    for(auto entry : sorted ) {
        if( entry.second.isFull() ) {
           // std::cerr << "split " << entry.second.items->size()<< " items at lvl " << level+1 << std::endl;
            splits( entry.second, dist, (level+1) );
        }
        else {
            dist.push_back( entry.second );
        }
    }

}

void inputDistribution(const std::string filename, int sz, std::vector<Hash::bucket> &distributed) {
    std::fstream in(filename);
    if( in.fail() ) {
        std::cerr << "Error opening file: " << filename << std::endl;
        exit(-2);
    }

    std::map<int, Hash::bucket> sorted;

    int level = 0, tmp_hash = 0;
    hashes.push_back( new Hash::Hash(256, 42454 ) );

    Hash::bucket b;
    Hash::int64 tmp = 0, tmp1 = 0, tmp2 = 0, tmp3 = 0;
    for (int i = 0; i < sz; i++) {
        in >> tmp;
        in >> tmp1;
        in >> tmp2;
        in >> tmp3;
        Hash::vector256 v(i, tmp, tmp1, tmp2, tmp3);

        tmp_hash = hashes[level]->hashVector( v );

        b = findOrCreateBucket( tmp_hash % MOD, sorted );
        b.insert( v );
    }
    std::cerr << "Read all vectors" << std::endl;
    level++;

    //splits
    for(auto entry : sorted) {
        if( entry.second.isFull() ) {
            splits( entry.second, distributed, level);
        }
        else distributed.push_back(entry.second);
    }

    std::cerr << "Distributed to buckets below " << Hash::THRESHOLD << " elements" << std::endl;
}

void readFile(const std::string filename,std::vector<Hash::vector256> &vectors) {
    std::fstream in(filename);

    int count = 0;
    Hash::int64 tmp, tmp1, tmp2, tmp3;
    while(in >> tmp && in >> tmp1 && in >> tmp2 && in >> tmp3) {
       // std::cerr << "V: " << tmp << " " << tmp1 << " " <<tmp2 << " " <<tmp3 << std::endl;
        vectors.push_back( Hash::vector256(count++, tmp, tmp1, tmp2, tmp3) );
    }
    std::cerr << "Read " << count << " vectors" << std::endl;

}

void bit_acc_test() {
    // bit access test
    Hash::vector256 v1(0, 1,2,4,8);
    Hash::vector256 v2(1, 8,4,2,1);
    std::cerr << " " << v1.is_set(0); //0-63
    std::cerr << " " << v1.is_set(65); //64-127
    std::cerr << " " << v1.is_set(130); //128-191
    std::cerr << " " << v1.is_set(195); //192-255
    std::cerr << std::endl;
    std::cerr << " " << v2.is_set(3); //0-63
    std::cerr << " " << v2.is_set(66); //64-127
    std::cerr << " " << v2.is_set(129); //128-191
    std::cerr << " " << v2.is_set(192); //192-255
    std::cerr << std::endl;
}

void corr_test() {
    Hash::vector256 v1(1,-4545410128706124432, 423485083363186329, 28290711320299080, -1687296419800652167);
    Hash::vector256 v2(2,-4256053852647568032, 428569225130022555, 28290711253190152, -1687296419734591911);
    std::cout << Hash::correlation(v1, v2) << std::endl;
}

void newBest(Hash::vector256 v1, Hash::vector256 v2, int corr, double j, Hash::Correlation &c) {
    c.vid1 = v1.vid;
    c.vid2 = v2.vid;
    c.sim = corr;
    c.jac = j;
}

int main(int argc, char** argv) {

    std::string fname;
    int nb_long, nb_vectors;

    fname = argv[1];
    nb_long = atoi(argv[2]);
    nb_vectors = atoi(argv[3]);

    std::cerr << "Args.. source file: " << fname;
    std::cerr << ", patch of " << nb_long << " long ints, total of " << nb_vectors << " vectors" << std::endl;

    //corr_test();

    //bits
    int bits = 64 * nb_long;

    //data structures
    std::vector<Hash::vector256> vectors;
    std::vector<Hash::bucket> dist;
    std::chrono::time_point<std::chrono::system_clock> t_distr, t_corr, t_end;

    t_distr = std::chrono::system_clock::now();
    inputDistribution(fname, nb_vectors, dist);

    Hash::Correlation best{0,0,0};

    t_corr = std::chrono::system_clock::now();
    int i = 0;
    for(auto b : dist) {
        //std::cerr << i++ << std::endl;

        for (int i = 0 ; i < b.items->size(); i++) {
            Hash::vector256 v1 = b.items->at(i);
            //#pragma omp parallel for shared(best)
            for (int j = i+1 ; j < b.items->size(); j++) {
                Hash::vector256 v2 = b.items->at(j);
                //block 0 and 2
                int corr = Hash::correlation(v1,v2,0);
                corr += Hash::correlation(v1, v2, 2);
                //if( corr < threshold ) continue;
                //all
                corr += Hash::correlation(v1, v2, 1);
                corr += Hash::correlation(v1, v2, 3);

                //#pragma omp critical
                if ( !best.isBest(corr) ) {
                    double jc = Hash::jac_similarity(v1, v2, corr);
                    newBest(v1,v2,corr,jc, best);
                } else if(best.sim == corr) {
                    double jc = Hash::jac_similarity(v1, v2, corr);
                    if( best.jac < jc ) {
                        newBest(v1,v2,corr,jc, best);
                    };
                }
            }
        }
    }
    t_end = std::chrono::system_clock::now();

    std::chrono::duration<double> td = t_corr - t_distr;
    std::chrono::duration<double> tc = t_end - t_corr;
    std::cerr << "Distribution time " << td.count() << ", Correlation time " << tc.count() << std::endl;


    best.print();
}