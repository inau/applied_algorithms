#ifndef CPP_IO_TRANSPOSE_H
#define CPP_IO_TRANSPOSE_H

#include "Matrix.h"

namespace Io {
    using namespace Io::Matrix;

    template <class T, int j>
    Matrix<T> transpose(Matrix<T> m) {
        Matrix<T> rm(m.dim());
        for (int i = 0; i < m.dim(); i++) {
            for (int j = 0; j < m.dim(); j++) {
                rm.set(i,j, m.get(i,j) );
            }
        }
        return rm;
    }

}

#endif