#include <iostream>
#include <c++/chrono>

#include "Matrix.h"
#include "MatrixMath.h"
#include "../mst/mRand.h"

void memory_layout(int seeda, int seedb, int dimension, int mla, int mlb) {
    std::string ma, mb;
    ma = mla == 0 ? "RM" : "CM";
    mb = mlb == 0 ? "RM" : "CM";
    std::cout << "#Trial N=" << dimension << " MEM LAYOUT:A=" << ma << ",B=" << mb << std::endl;
    std::cout << "#Type    \tTime" << std::endl;
    int seedsa[dimension];
    int seedsb[dimension];

    mRand::genSeeds(seeda, dimension, seedsa);
    mRand::genSeeds(seedb, dimension, seedsb);

    // My Matrix
    Io::Matrix::Matrix<int> A(dimension, mla);
    Io::Matrix::Matrix<int> B(dimension, mlb);

    for (int i = 0; i < dimension; i++) {
        for (int j = 0; j < dimension; j++) {
            A.set(i, j, mRand::getEdgeWeight(i, j, seedsa));
            B.set(i, j, mRand::getEdgeWeight(i, j, seedsb));
        }
    }
    std::chrono::time_point<std::chrono::system_clock> start, stop;
    for (int k = 0; k < 32; k *= 2) {
        if (k > dimension) break;
        start = std::chrono::system_clock::now();
        if (k == 0) {
            std::cout << "Naive   ";
            Io::MatrixMath::multiply(A, B);
            k++;
        } else {
            std::cout << "Tiled=" << k << " ";
            Io::MatrixMath::tiles(k, A, B);
        }
        stop = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed = stop - start;
        std::cout << "\t" << elapsed.count() << std::endl;
    }
}

void trial(int seeda, int seedb, int dimension) {
    std::cout << "#Trial "<< dimension << "x" << dimension << std::endl;
    std::cout << "#Type    \tTime" << std::endl;
    int seedsa[dimension];
    int seedsb[dimension];

    mRand::genSeeds(seeda, dimension, seedsa);
    mRand::genSeeds(seedb, dimension, seedsb);

    // My Matrix
    Io::Matrix::Matrix<int> A(dimension, Io::Matrix::ROW_MAJOR);
    Io::Matrix::Matrix<int> B(dimension, Io::Matrix::COL_MAJOR);

    for (int i = 0; i < dimension; i++) {
        for (int j = 0; j < dimension; j++) {
            A.set(i,j, mRand::getEdgeWeight(i,j,seedsa));
            B.set(i,j, mRand::getEdgeWeight(i,j,seedsb));
        }
    }
    std::chrono::time_point<std::chrono::system_clock> start, stop;
    for (int k = 0; k < 32; k*=2) {
        if(k > dimension) break;
        start = std::chrono::system_clock::now();
        if(k == 0) {
            std::cout << "Naive   ";
            Io::MatrixMath::multiply(A,B);
            k++;
        }
        else {
            std::cout << "Tiled=" << k << " ";
            Io::MatrixMath::tiles(k,A,B);
        }
        stop = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed = stop-start;
        std::cout << "\t" << elapsed.count() << std::endl;
    }


}

int main(int argc, char** argv) {
    int n = 0;
    int seeda = 0;
    int seedb = 0;
    std::cin >> n;
    std::cin >> seeda;
    std::cin >> seedb;

    for (int tilesz = 1; tilesz <= n; tilesz *= 2) {
 //       trial(seeda, seedb, tilesz);
        memory_layout(seeda, seedb, tilesz, Io::Matrix::ROW_MAJOR, Io::Matrix::ROW_MAJOR);
        memory_layout(seeda, seedb, tilesz, Io::Matrix::ROW_MAJOR, Io::Matrix::COL_MAJOR);
    }

 }