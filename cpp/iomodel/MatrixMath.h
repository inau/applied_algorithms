#ifndef CPP_IO_MATRIX_MATH_CPP
#define CPP_IO_MATRIX_MATH_CPP

#include "Matrix.h"
#include <iostream>


namespace Io {
    namespace MatrixMath {

        template<class T>
        Io::Matrix::Matrix<T> add(Io::Matrix::Matrix<T> &_this, Io::Matrix::Matrix<T> &other) {
            if (_this.dim() == other.dim()) {
                //do result matrix
                Io::Matrix::Matrix<T> tmp(_this.dim());
                for (int i = 0; i < _this.dim(); i++) {
                    for (int j = 0; j < _this.dim(); j++) {
                        tmp.set(i, j, 0);
                        tmp.incr(i, j, _this.get(i, j) * other.get(i, j));
                    }
                }
                return tmp;
            } else {
                std::cerr << "multiplying uneven matrices which this function does not support" << std::endl;
                exit(-1);
            }
        }

        template<class T>
        Io::Matrix::Matrix<T> multiply(Io::Matrix::Matrix<T> &_this, Io::Matrix::Matrix<T> &other) {
            //equal sized matrices
            if (_this.dim() == other.dim()) {
                //do result matrix
                Io::Matrix::Matrix<T> tmp(_this.dim());
                #pragma omp parallel for collapse(2)
                for (int i = 0; i < _this.dim(); i++) {
                    for (int j = 0; j < _this.dim(); j++) {
                        tmp.set(i, j, 0);

                        for (int k = 0; k < _this.dim(); k++) {
                            tmp.incr(i, j, _this.get(i, k) * other.get(k, j));
                        }
                    }
                }
                return tmp;
            } else {
                std::cerr << "multiplying uneven matrices which this function does not support" << std::endl;
                exit(-1);
            }
        }

        template<class T>
        void tiled_multiply(Io::Matrix::offset tile, Io::Matrix::Matrix<T> &_this, Io::Matrix::Matrix<T> &other,
                            Io::Matrix::Matrix<T> &target) {
            //equal sized matrices
            if (_this.dim() == other.dim()) {
                //do result matrix
                for (int i = tile.row; i < tile.row+tile.block_sz; i++) {
                    for (int j = tile.col; j < tile.col+tile.block_sz; j++) {
                        target.set(i, j, 0);
                        for (int k = 0; k < _this.dim(); k++) {
                            target.incr(i, j, _this.get(i, k) * other.get(k, j));
                        }
                    }
                }
            } else {
                std::cerr << "multiplying uneven matrices which this function does not support" << std::endl;
                exit(-1);
            }
        }

        template<class T>
        Io::Matrix::Matrix<T> tiles(int N_split, Io::Matrix::Matrix<T> A, Io::Matrix::Matrix<T> B) {
            if (A.dim() != B.dim()) {
                std::cerr << "multiplying uneven matrices which this function does not support" << std::endl;
                exit(-1);
            }

            Io::Matrix::Matrix<T> C(A.dim());
            int ndim = A.dim() / N_split;

            #pragma omp parallel for collapse(2)
            for (int i = 0; i < N_split; i++) {
                for (int j = 0; j < N_split; j++) {
                    tiled_multiply(Io::Matrix::offset(i * ndim, j * ndim, ndim), A, B, C);
                }
            }
            return C;
        }
    }
}

#endif