#ifndef APPLIED_ALGORITHMS_CPP_IO_MATRIX_H
#define APPLIED_ALGORITHMS_CPP_IO_MATRIX_H

namespace Io {

    namespace Matrix {

        //assumption is matrices of equal dimentions(NxN)

        /**
         * Access methods for column major
         * Traversing cols before rows
         * @param i Row
         * @param j Col
         * @param dim Size of square
         * @return index in a square of dim x dim
         */
        static int cm(int i, int j, int dim) {
            return i*dim + j;
        }

        /**
         * Access methods for row major
         * Traversing rows before cols
         * @param i Row
         * @param j Col
         * @param dim Size of square
         * @return index in a square of dim x dim
         */
        static int rm(int i, int j, int dim) {
            return i + dim*j;
        }

        static const int ROW_MAJOR = 0;
        static const int COL_MAJOR = 1;

        /**
         * Square matrix with elements of type T
         * @tparam T
         */
        template <class T>
        class Matrix {
        public:
            Matrix(int N, int access_mode = ROW_MAJOR) {
                this->access_type = access_mode;
                this->N = N;
                data = new T[N*N];
            }
            T get(int i, int j) {
                return data[ local_index(i,j) ];
            };
            void set(int i, int j, T val) {
                data[ local_index(i,j) ] = val;
            };
            void incr(int i, int j, T val) {
                data[ local_index(i,j) ] += val;
            };

            int dim() {
                return N;
            };
            int type() {
                return access_type;
            }

            void print(std::ostream &out) {
                out << "[" << this->N << "," << this->N << "](";
                for (int i = 0; i < this->N; i++) {
                    out << "(";
                    for (int j = 0; j < this->N; j++) {
                        out << this->get(i, j) << " ";
                    }
                    out << "),";
                }
                out << ")" << std::endl;
            }

        protected:
            int local_index(int i, int j) {
                if( this->access_type == 0) rm(i,j, dim());
                else cm(i,j,dim());
            };
            int N;
            int access_type;
            T *data;
        };

/**
        template <class T, int t>
        Matrix<T,t> add(Matrix<T,t> &A, Matrix<T,t> &B) {
            if (A.dim() && B.dim()) {
                int t = A.type();
                Matrix<T,t> C(A.dim());
                for (int i = 0; i < A.dim(); i++)
                    for (int j = 0; j < B.dim(); j++) {
                        C.incr(i,j, A.get(i,j) + B.get(i,j));
                    }

                return C;
            }
            else {
                std::cerr << "unable to manipulate matrices of unequal sizes";
                exit(-1);
            }
        };

        template <class T>
        Matrix<T> mul(Matrix<T> &A, Matrix<T> &B) {
            return A._mul(B);
        };
**/
        // Matrix Views
        struct offset {
            int col,row, block_sz;
            offset(int i, int j, int sz) : col(i), row(j), block_sz(sz) {};
        };

        /**
         * Overwrite a section in target Matrix with the data from the provided chunk
         * @tparam T
         * @param chunk
         * @param tar
         * @param section
         */
        template <class T>
        void write_chunk(Matrix<T> chunk, Matrix<T> tar, offset section) {
            for (int i = 0; i < chunk.dim(); i++) {
                for (int j = 0; j < chunk.dim(); j++) {
                    tar.set( section.row + i, section.col + j, chunk.get(i,j) );
                }
            }
        }

    }
}

#endif
