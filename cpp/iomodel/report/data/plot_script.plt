# PLOT 2 (lines)
set title 'Tests Memory Layout RM RM'

#terminal = output to
set term png enhanced

set xlabel "input size n"
set ylabel "time in s"

set xtics ('128' 0,'256' 1,'512' 2,'1024' 3,'2048' 4)
set ytics 2

set key t l

set logscale y 2

set grid

set xrange [-.1:3.1]
set yrange [0.01:33]
set ticslevel 0
#data input
file1 = "data.txt"
set output 'rm_rm.png'

plot file1 every 10::0 u 0:2 w lp title "Naive-OpenMP" lw 1,\
	 file1 every 10::1 u 0:2 w lp title "Tiled2-OpenMP" lw 1,\
	 file1 every 10::2 u 0:2 w lp title "Tiled4-OpenMP" lw 1,\
	 file1 every 10::3 u 0:2 w lp title "Tiled8-OpenMP" lw 1,\
	 file1 every 10::4 u 0:2 w lp title "Tiled16-OpenMP" lw 1

	 set title 'Tests Memory Layout RM CM'
set output 'rm_cm.png'

plot file1 every 10::5 u 0:2 w lp title "Naive-OpenMP" lw 1,\
	 file1 every 10::6 u 0:2 w lp title "Tiled2-OpenMP" lw 1,\
	 file1 every 10::7 u 0:2 w lp title "Tiled4-OpenMP" lw 1,\
	 file1 every 10::8 u 0:2 w lp title "Tiled8-OpenMP" lw 1,\
	 file1 every 10::9 u 0:2 w lp title "Tiled16-OpenMP" lw 1

	 
	 set title 'Tests Memory Layout Naive'
set output 'naive_compare.png'

plot file1 every 10::0 u 0:2 w lp title "Naive-RM-RM" lw 1,\
	 file1 every 10::5 u 0:2 w lp title "Naive-RM-CM" lw 1
	 