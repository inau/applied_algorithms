//
// Created by ivan_ on 07-Sep-17.
//

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <unordered_set>
#include <x86intrin.h>
#include <cmath>
#include <vector>
#include <bitset>

//alias
double ln(double x) {
    return std::log(x);
}

int distinct_integers() {
    std::unordered_set<int> set = {};

    int i;
    while( std::cin >> i ) set.insert(i);

    return set.size();
}

unsigned int A[] = {
        0x21ae4036, 0x32435171, 0xac3338cf, 0xea97b40c,
        0x0e504b22, 0x9ff9a4ef, 0x111d014d, 0x934f3787,
        0x6cd079bf, 0x69db5c31, 0xdf3c28ed, 0x40daf2ad,
        0x82a5891c, 0x4659c7b0, 0x73dc0ca8, 0xdad3aca2,
        0x00c74c7e, 0x9a2521e2, 0xf38eb6aa, 0x64711ab6,
        0x5823150a, 0xd13a3a9a, 0x30a5aa04, 0x0fb9a1da,
        0xef785119, 0xc9f0b067, 0x1e7dde42, 0xdda4a7b2,
        0x1a1c2640, 0x297c0633, 0x744edb48, 0x19adce93
};

int f(int x) {
    return  ((x*0xbc164501) & 0x7fe00000) >> 21;
}

//leading zeroes (clz = count leading zeroes) (starting with atleast 1)
int rho(int x) {
    return 1 + __builtin_clz(x);
}

int H(int x, int bits) {
    int res = 0;
    for(int i = bits; i >= 0; i--) {
        res += ( ( _popcnt32(A[i] & x) & 1) << (bits-i));
    }
    return res;
}

int h(int x) {
    return H(x, 31);
}

double mpow2(double exponent) {
    return std::pow(2.0, exponent);
}

//harmonic mean
double Z(int M[], int m) {
    double z = 0.0;
    //std::cerr << "Z = 1.0 / ( ";
    for(int i = 0; i < m; i++) {
        //std::cerr << " + 2^(" << -M[i] <<")";
        z += mpow2(-M[i]);
    }
//    std::cerr << " )" << std::endl;
    return 1.0/z;
}

double Estimate(int m) {
    int M[m] = {0};
    //number of m entries which are 0
    double V = m;
    int j, in;
    while(std::cin >> in) {
        j = f( in );
//        std::cerr << "M[F("<<i<<"): " << j << "] = Max( rho " << rho( h(Y[i]) ) << ", curr: " << M[j] << ")" << std::endl;
        int r = rho( h( in ) );
        if(M[j] == 0) {
            if(r != 0) {
                M[j] = std::max(M[j], r );
                V--;
            }
            //else both == 0
        }
            //both entry and rho != 0 and hence max
        else M[j] = std::max(M[j], r );
    }
    double z = Z(M, m);
//    std::cerr << "Z = " << z << std::endl;
    double E = m*m*z*0.7213/(1 + 1.079/m);
//    std::cerr << "E = " << E << std::endl;
    if(E < 2.5*m && V > 0) E = m * ln( m/V );
    return E;
}

void readInAndApplyEstimate(int threshold, int m = 1024) {
    int i;
    double E = Estimate( m );
    std::string s;
    s = (E < threshold) ? "below" : "above";
    std::cout << s;
}

void readIn() {
    int i;
    while(std::cin >> i) {
        std::cout << h(i) << std::endl;
    }
}

int main() {
    int th = 0;
    std::cin >> th;
    readInAndApplyEstimate(th);
}