//
// Created by Ivan on 07-Nov-17.
//
#include "sequence.h"

/**
 * Prepend character for 0,0
 * @param raw
 * @return
 */
char* getWord(char *raw) {
    char *ret = new char[ strlen(raw) + 1 ];
    ret[0] = '-';
    for (int i = 0; i < strlen(raw); i++) {
        ret[i+1] = raw[i];
    }
    return ret;
}

int main(int argc, char** argv) {

    int *row = new int[4]{0,2,4,6};
    int *next;
    char *a = "-abc";
    char *b = "-abd";

    std::cerr << "  ";
    for (int j = 0; j < 4; j++) {
        std::cerr << a[j] << " ";
    }
    std::cerr <<std::endl << b[0] << " ";
    for (int j = 0; j < 4; j++) {
        std::cerr << row[j] << " ";
    }
    std::cerr << std::endl;


    for (int i = 1; i < 4; i++) {
        next = sequence_alignment::cost_row(a,b,row, 4, i);
        row = next;
        std::cerr << b[i] << " ";
        for (int j = 0; j < 4; j++) {
            std::cerr << row[j] << " ";
        }
        std::cerr << std::endl;
    }
    /**
    std::cerr << "compare:" <<std::endl;
    char* A = getWord( argv[1] );
    char* B = getWord( argv[2] );

    std::cerr << A << " to " << B <<std::endl;


    sequence_alignment::Sequence res = sequence_alignment::align(A,B, sequence_alignment::MEM_EFFICIENT, sequence_alignment::output::INORDER);

    sequence_alignment::output::print(std::cout, res); //sequence_alignment::output::REVERSE
//    sequence_alignment::output::print(std::cout, res, sequence_alignment::output::INORDER);
**/
}