//
// Created by Ivan on 07-Nov-17.
//

#ifndef APPLIED_ALGORITHMS_CPP_SEQUENCE_H
#define APPLIED_ALGORITHMS_CPP_SEQUENCE_H

#include <vector>
#include <ostream>
#include <cmath>
#include <iostream>
#include <string.h>

namespace sequence_alignment {
    typedef std::vector<char> Sequence;

    namespace output {
        const int REVERSE = 1, INORDER = -1;
        const char ONE = 'a', TWO = 'b', BOTH = '|';

        void print(std::ostream &out, Sequence &res, int order = REVERSE) {
            int cst;
            switch (order) {
                case REVERSE:
                    cst = res.size()-1;
                    break;
                case INORDER:
                    cst = 0;
                    break;
            }
            for (int i = res.size()-1; i >= 0 ; i--) {
                std::cout << res.at( abs(cst - (order*i)) );
            }
            std::cout << std::endl;
        }
    }

    const int MEM_EFFICIENT = 1, FULL_TABLE = 0;

    const int GAP_COST = 2;

    int min(int a, int b) {
        return  a < b ? a : b;
    }
    int min_eq(int a, int b) {
        return  a <= b ? a : b;
    }
    int char_dist(char a, char b) {
        return a == b ? 0 : GAP_COST;
    }

    class COST_TABLE {
    protected:
        int *cost;

    public:
        const int M,N;

        void set(int i, int j, int c) {
            cost[ i + (j*M) ] = c;
        }
        int get(int i, int j) {
            return cost[ i + (j*M) ];
        }
        void print(std::ostream &str) {
            for (int i = 0; i < N; ++i) {
                for (int j = 0; j < M; j++) {
                    str << get(j,i) << " ";
                }
                str << std::endl;
            }
        }
        COST_TABLE(int size_a, int size_b) : M(size_a), N(size_b) {
            std::cerr << "Table of "<<M<<" x "<<N<<std::endl;
            cost = new int[M*N];
        }
    };

    class COST_ROWS {
    protected:
        const int M,N;
        int count = 0;
        int *cached_row, *next_r;
        const char *a,*b;

        //column traversal
        int cost_of(int i) {
            if(i == 0) {
                return count*GAP_COST;
            } else {
                //std::cerr << "? " << a[i] << " == " << b[count] << std::endl;
                int diagonal = cached_row[i-1] + char_dist(a[i], b[count]);
                int top = cached_row[i] + GAP_COST;
                int left = next_r[i-1] + GAP_COST;

                //min of 3 vals
                //std::cerr << "? DIA " << min_eq(top,left) << " , TOP " << top << " , LEFT " << left << std::endl;
                return min( min_eq(top,left), diagonal);
            }
        }

        bool step() {
            if(count == N) {
                return false;
            }
            std::cerr << ":::" << std::endl;

            int next_row[M];
            //initialize first row
            if(count == 0) {
                for (int i = 0; i < M; i++) {
                    next_row[i] = i * GAP_COST;
                    std::cerr << next_row[i] << " ";
                }
                count++;
                //cache
                next_r = next_row;
                std::cerr << std::endl;
            }
            cached_row = next_r;

            //calculate next row
            for (int i = 0; i < M; i++) {
                next_row[i] = cost_of(i);
                //std::cerr << next_row[i] << " ";
            }
            for (int i = 0; i < M; i++) {
                std::cerr << next_row[i] << " ";
            }
            count++;
            next_r = next_row;
            std::cerr << std::endl;
            return true;
        }

    public:
        /**
         * Construct object and calculate first two rows
         * @param x word to compare
         * @param y word to compare
         */
        COST_ROWS(char* x, char* y) : a(x), b(y), M(strlen(x)), N(strlen(y)) {
            std::cerr << "Two rows for "<<M<<" x "<<N<< " alignment"<<std::endl;
            nextTier();
        }

        bool nextTier() {
            return step();
        }

        int* previous_row() {
            return cached_row;
        }
        int* current_row() {
            return next_r;
        }
    };


    char decide_char(int i, int j, char* X, char *Y, COST_ROWS &cost) {
        //diagonal
        int dia = cost.current_row()[i+1];
        int right = cost.previous_row()[i+1];
        int below = cost.current_row()[i];

        if( dia > right || dia > below ) {
            if( right <= below ) {
                return sequence_alignment::output::ONE;
            } else return sequence_alignment::output::TWO;
        }
        else return sequence_alignment::output::BOTH;
    }

    int* cost_row(char *a, char *b, int *prev, int col_sz, int row_nb) {
        int *next = new int[col_sz];
        next[0] = row_nb * GAP_COST;
        int align, del, ins;
        for (int i = 1; i < col_sz; i++) {
            align = prev[i-1] + char_dist(a[i],b[row_nb]);
            ins = prev[i] + GAP_COST;
            del = next[i-1] + GAP_COST;

            next[i] = min( min_eq(del,ins), align );
        }
        return next;
    }

    Sequence sparse_alignment(char* X, char* Y) {
        int M = strlen(X);
        int N = strlen(Y);
        std::cerr << "Memory efficient alignment M = " << M << ", N = " << N << std::endl;
        Sequence s;
        COST_ROWS cost(X,Y);
        int i = 0;
        int j = 0;
        char c;
        while( i < M || j < N ) {
            c = decide_char(i,j,X,Y,cost);
            switch (c) {
                case sequence_alignment::output::BOTH:
                    i++; j++;
                    break;
                case sequence_alignment::output::ONE:
                    i++;
                    break;
                case sequence_alignment::output::TWO:
                    j++;
                    break;
            }
            s.push_back( c );
            cost.nextTier();


            if( j == N  ) {
                break;
            }
            /**
                while ( i < M ){
                    s.push_back( sequence_alignment::output::ONE );
                    i++;
                }
            }
            if( i == M  ) {
                while ( j < N ){
                    j++;
                    s.push_back( sequence_alignment::output::TWO );
                }
            }
             **/
            std::cerr << "( " << i << ", " << j << " )" << std::endl;
        }
        return s;
    }

    Sequence reverse_path(int M, int N, COST_TABLE &cost) {
        std::vector<char> sequence;

        //shortest path (reverse)
        for (int i = M; i > 0;) {
            int c1,c2,cb,mc;
            char cc;
            for (int j = N; j > 0;) {
                cb = cost.get(i-1,j-1);
                c1 = cost.get(i-1,j);
                c2 = cost.get(i,j-1);

                if( c1 <= c2 ) {
                    mc = c1;
                    cc = output::ONE;
                }
                else {
                    mc = c2;
                    cc = output::TWO;
                }

                if (cb <= mc) {
                    mc = cb;
                    cc = output::BOTH;
                }

                sequence.push_back(cc);

                switch(cc) {
                    case output::BOTH:
                        i--;j--;
                        break;
                    case output::ONE:
                        i--;
                        break;
                    case output::TWO:
                        j--;
                        break;
                }
            }
        }
        return sequence;
    }

    Sequence path(int M, int N, COST_TABLE &cost) {
        std::vector<char> sequence;

        //shortest path (reverse)
        for (int i = 0; i < M-1;) {
            int c1,c2,cb,mc;
            char cc;
            for (int j = 0; j < N-1;) {
                cb = cost.get(i+1,j+1);
                c1 = cost.get(i+1,j);
                c2 = cost.get(i,j+1);

                if( c1 <= c2 ) {
                    mc = c1;
                    cc = output::ONE;
                }
                else {
                    mc = c2;
                    cc = output::TWO;
                }

                if (cb <= mc) {
                    mc = cb;
                    cc = output::BOTH;
                }

                sequence.push_back(cc);

                switch(cc) {
                    case output::BOTH:
                        i++;j++;
                        break;
                    case output::ONE:
                        i++;
                        break;
                    case output::TWO:
                        j++;
                        break;
                }
            }
        }
        return sequence;
    }

    /**
     * Optimal calculation
     */

    int OPT(int i, int j, COST_TABLE &cost) {
        if(i >= 1 && j >= 1) {
            int a = cost.get(i,j) + OPT(i-1, j-1, cost);
            int b = GAP_COST + OPT(i-1, j, cost);
            int c = GAP_COST + OPT(i, j-1, cost);

            int min = a <= b ? a : b;

            min = min < c ? min : c;
            return min;
        }
        else return cost.get(i,j);

    }

    Sequence alignment(char* X, char* Y, int traversal_order = output::REVERSE) {
        int x_len = strlen(X);
        int y_len = strlen(Y);

        std::cerr << "Length M="<<x_len<<" N="<<y_len<<std::endl;
        COST_TABLE cost(x_len, y_len);
        for (int x = 0; x < x_len; x++) cost.set(x,0, GAP_COST * x);
        for (int y = 0; y < x_len; y++) cost.set(0,y, GAP_COST * y);
        //std::cerr << "Init rows"<<std::endl;

        //cost table
        for (int j = 1; j < y_len; j++) {
            for (int i = 1; i < x_len; i++) {
                cost.set(i,j, OPT(i,j, cost) );
            }
        }

        cost.print( std::cerr );

        switch ( traversal_order ) {
            case output::INORDER:
                return path(x_len, y_len, cost);
            case output::REVERSE:
                return reverse_path(x_len,y_len,cost);
        }
    }

    Sequence align(char* A, char* B, int TYPE = FULL_TABLE, int traversal_order = output::REVERSE) {
            switch (TYPE) {
                case MEM_EFFICIENT:
                    return sparse_alignment(A,B);
                case FULL_TABLE:
                    return alignment(A,B,traversal_order);
                default:
                    return Sequence{1};
            }
        }


}
#endif //APPLIED_ALGORITHMS_CPP_SEQUENCE_H
