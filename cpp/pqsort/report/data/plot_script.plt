# PLOT 2 (lines)
set title 'Linear Increment'

#terminal = output to
set term png enhanced

set xlabel "input size n"
set ylabel "time in s"

set xtics 1000000
set ytics 0.5

set grid 

set xrange [200000:4000000]
set yrange [0:6]
set ticslevel 0
#data input
set output 'linear_inorder.png'

file1 = "lin2.txt"
plot file1 every ::0::19 using 1:2 w lp title "PQ inorder" lw 1,\
file1 every ::0::19 using 1:3 w lp title "LibSort inorder" lw 1

set output 'linear_reverse.png'
plot file1 every ::20::39 using 1:2 w lp title "PQ reverse" lw 1,\
	file1 every ::20::39 using 1:3 w lp title "LibSort reverse" lw 1
	
set output 'linear_rd.png'
plot file1 every ::40::59 using 1:2 w lp title "PQ random" lw 1,\
	file1 every ::40::59 using 1:3 w lp title "LibSort random" lw 1
	
set xrange [20000:10300000]
set yrange [0:10]
set xtics 1000000
set ytics 1
set title 'Doubling Increment'
set logscale x 2
#set logscale y 10
set xlabel "input size lg(n)"
set ylabel "time in s"
	
file2 = "fresh.txt"
set output 'doubling_inorder.png'
plot file2 every ::0::9 using 1:2 w lp title "PQ inorder" lw 1,\
file2 every ::0::9 using 1:3 w lp title "LibSort inorder" lw 1

set output 'doubling_reverse.png'
plot file2 every ::10::19 using 1:2 w lp title "PQ reverse" lw 1,\
	file2 every ::10::19 using 1:3 w lp title "LibSort reverse" lw 1
	
set output 'doubling_rd.png'
plot file2 every ::20::29 using 1:2 w lp title "PQ random" lw 1,\
	file2 every ::20::29 using 1:3 w lp title "LibSort random" lw 1	