\documentclass[titlepage]{article}
\usepackage[margin=3cm]{geometry}
\usepackage{titlepic}
\usepackage{graphicx}
\usepackage{pdfpages}
\usepackage{listings, lstautogobble}
\usepackage{color}
\usepackage{verbatim}
\usepackage[hidelinks]{hyperref}
\usepackage[parfill]{parskip}

%code block setup
\lstset{
	language=c++,
	breaklines=true, breakatwhitespace=true,
	numbers=left, numberstyle=\color{gray},
	keywordstyle=\color{blue},
	%title=\textbf{Source:} \lstname,
	basicstyle=\scriptsize\ttfamily,
    	commentstyle=\ttfamily\itshape\color{gray},
    	stringstyle=\ttfamily, showstringspaces=false,
    	tabsize=2,
    	xleftmargin=25pt, xrightmargin=25pt
}

\newcommand{\codefile}[2]{
	\lstinputlisting[caption={#1}\label{#2}]{#1}
}
\newcommand{\codebit}[4]{
	\lstinputlisting[caption={#1}\label{#4}, firstnumber=#2, firstline=#2, lastline=#3]{#1}
}

\begin{document}

%\includepdf{img/itu_front.pdf}
\begin{titlepage}
	\centering

	{\huge\bfseries Applied Algorithms \par}
	\vspace{2cm}
	{\Large Ivan Naumovski\\{inau@itu.dk}\par}
	\vfill

	\vfill
	\includegraphics[width=0.3\textwidth]{img/itulogo.jpg}\par\vspace{1cm}
% Bottom of the page
	{\large \today\par}
\end{titlepage}

\pagenumbering{gobble}
%\maketitle
\clearpage
\tableofcontents
\clearpage

\pagenumbering{arabic}
\section{Sorting Case Study}
I will conduct a study on sorting in C++ using two different native approaches.\\
One is a using the priority queue from the standard lib in the $<queue>$ libraries. The PQ is a data structure that keeps the elements in an order as specified by a given comparator.\\
The other approach is using an array or other similar structure and apply the default library sorting algorithms found in $<algorithms> $ library to the structure.\\
Sorting the same items using same ordering, $E_i < E_j$, should yield the same results.

\begin{tabular}{l l}
\hline \hline
Question & How well do the different structures scale on increasing input size of certain types.\\
Indicator & Running Time\\
Factors & Random sequences as well as deterministic sequences of integers.\\
Levels  & n= 200k ... 4000k, increment by 200k\\
Trials & 1\\
Design points & in-order, reverse order and random sequences with input of above levels\\
&(20 times per variant for a total of 60 measurements)\\
Outputs & input size, wall clock time of PQ, wall clock time libsort.\\ \hline
\end{tabular}

The study will compare how well the different sorting approaches handle different variants of input.
While the priority queue keeps the content in order this comes at a cost.
Everytime something is inserted into the PQ, a reordering will happen.
This can be done using structures such as a heap as underlying container - in certain instances with ordered input this will perform very well, in others it can force the container to reorder a bunch of items in the container matching the height.

The C++ documentation states that a heap is used as the underlying structure for the PQ, which usually indicates that insert operations are worst case $O(lg \cdot n)$ and best case would be $O(1)$.
Doing n inserts would mean a complexity of $n \cdot O(lg \cdot n)$.\\
The C++ documentation for the library sort states that gcc uses IntroSort as its default sorting algorithm which should have a similar complexity both in worst and average case of $n \cdot O(lg \cdot n)$.

The study will explore how well the two sorting approaches scale in terms of \textit{running time} once input size grows using \textit{wall clock time}.
Furthermore it will explore whether certain types of inputs have an impact on how performant the compared structures are.
\textit{\textbf{The claim based on the documentation is that both approaches should perform equally good in the worst case in regards to running times.}}

The instance parameters will be varied such that input is given randomized, in order and in reverse order.
Furthermore the input sizes will be gradually incremented both linearly and doubled.

I have run the tests on a machine with the specs as seen on listing \ref{dx}.
\codebit{data/DxDiag.txt}{7}{14}{dx}

\subsection{Experimental Setup}
The testbed consists of three classes. A base class that generates and inserts input into the structures, as well as a timing mechanims which tracks wall clock time during the lifecycle of the class instance.
And two sub classes which extend the base with priority queue and vector data structures as containers.

The input generation for the ordered inputs is done using simple for loops and the random input is done using a generator which is the one provided in the cpp $<random>$ package.\\
Additionally the random generator takes a seed as input to ensure that one can generate the same sequence both for the library sort and priority queue test cases.

Moreover a validation of the results is done post timing measurements using a comparison of the ordering, such that $\left\lbrace PQ_{j=0..N}, LIB_{j=0..N} | PQ_j = LIB_j \right\rbrace$.
This is to confirm that both approaches also compute the same result for the same case.

An excerpt of the code used to generate test data can be seen on listing \ref{cgen}.
The testbed can be improved by running multiple tests with the same size and data structure and do averages rather than a single test per size and structure.

\codebit{../PqExp.h}{22}{62}{cgen}

%\codebit{src/sumofdeg/sum.cpp}{55}{77}

\pagebreak
\subsection{Tests}
\subsubsection*{linear N, Step 200k}
A straightforward linear scaling with steps of 200k with 20 tests.
\begin{figure}[h]
\centering
\includegraphics[width=.495\textwidth]{data/linear_inorder.png}
\includegraphics[width=.495\textwidth]{data/linear_reverse.png}
\includegraphics[scale=.495]{data/linear_rd.png}
\end{figure}

%\subsubsection*{Doubling of N}
%\begin{figure}[h]
%\centering
%\includegraphics[width=.495\textwidth]{data/doubling_inorder.png}
%\includegraphics[width=.495\textwidth]{data/doubling_reverse.png}
%\includegraphics[scale=.495]{data/doubling_rd.png}
%\end{figure}

%The doubling experiments are depicted using a logarithmic x-axis with base 2.

\subsection{Conclusion}
When incrementing input size linearly with steps of 200k the time which it takes to process the input also increases.
The measurement is done using wall clock time.
What is interesting is how much the order of input affects the different structures.\\
The library sort on the vector is by far the fastest compared to a PQ.
This is due to everytime one removes the top element the queue needs to satisfy the property that the top element is indeed the smallest in the structure.

The PQ performs fastest (in respect to itself) when the data is ordered whereas the cases with random data and reverse data perform equally poor with not too much time difference.
A insert always happens at the end of the structure, then all the reordering happens and can take up to $O(lg * N)$.
When input is in the correct ordering one comparison is enough to confirm that the item is at the correct place, however when the input is reversed it will always trigger the entire heap traversal(tree depth).\\
The top operation is constant for a PQ, the reordering is expensive.

The library sort performs better compared to the PQ in every case.\\

The claim was that the two approaches due to them having similar time complexities should be somewhat equal.
This is however not the case, which opens up for questions whether the memory architecture and different levels of I/O have an impact on performance.

\pagebreak
\subsection{Raw data}
\subsubsection*{Linear}
\verbatiminput{data/lin2.txt}
%\subsubsection*{Doubling}
%\verbatiminput{data/fresh.txt}

\end{document}
