//
// Created by ivan_ on 21-Sep-17.
//

#include "PqExp.h"
bool compare(PqExp *pq, VectorExp *vq) {
    vector<int>::iterator it;
    for (it = vq->getVector()->begin(); it < vq->getVector()->end(); it++) {
        if(*it != pq->getQueue()->top()) return false;
        pq->getQueue()->pop();
    }
    return true;
}

struct TestType {
//Linear = 0 Doubling = 1;
    int t;
    int stepsz;

    int step(int i) {
        switch(t) {
            case 0:
                return i*stepsz;
            case 1:
                int d = stepsz;
                for (int j = 1; j <= i; j++) {
                    d = d*2;
                }
                return d;
        }
    }
};
TestType tt;

void randomtest(int test) {
    std::cout << "# Random input" << std::endl;
    std::cout << "# N\tPrioQ(s)\tLibSort(s)\tsame?" << std::endl;
    double t1,t2;
    int s;
    std::string same;
    for (int i = 1; i <= test; i++) {
        s = tt.step(i);
        PqExp pq = PqExp();
        pq.random_input(s, 42);
        while( !pq.getQueue()->empty() ) pq.getQueue()->pop();
        t1 = pq.elapsed();
        std::cout << s << "\t" << t1 << "\t";
        VectorExp vq = VectorExp();
        vq.random_input(s, 42);
        vq.reset_sw();
        vq.sort();
        t2 = vq.elapsed();
        same = "true"; //compare(&pq,&vq) ? "true" : "false";
        std::cout << t2 << "\t" << same ;
        std::cout << std::endl;
    }
}

void reversetest(int test) {
    std::cout << "# Reverse (high to low) input" << std::endl;
    std::cout << "# N\tPrioQ(s)\tLibSort(s)\tsame?" << std::endl;
    double t1,t2;
    int s;
    std::string same;
    for (int i = 1; i <= test; i++) {
        s = tt.step(i);
        PqExp pq = PqExp();
        pq.reversed_input(s);
        while( !pq.getQueue()->empty() ) pq.getQueue()->pop();
        t1 = pq.elapsed();
        std::cout << s << "\t" << t1 << "\t";
        VectorExp vq = VectorExp();
        vq.reversed_input(s);
        vq.reset_sw();
        vq.sort();
        t2 = vq.elapsed();
        same = "true"; //compare(&pq,&vq) ? "true" : "false";
        std::cout << t2 << "\t" << same ;
        std::cout << std::endl;
    }
}

void inordertest(int test) {
    std::cout << "# Inorder (low to high) input" << std::endl;
    std::cout << "# N\tPrioQ(s)\tLibSort(s)\tsame?" << std::endl;
    double t1,t2;
    int s;
    std::string same;
    for (int i = 1; i <= test; i++) {
        s = tt.step(i);
        PqExp pq = PqExp();
        pq.inorder_input(s);
        while( !pq.getQueue()->empty() ) pq.getQueue()->pop();
        t1 = pq.elapsed();
        std::cout << s << "\t" << t1 << "\t";
        VectorExp vq = VectorExp();
        vq.inorder_input(s);
        vq.reset_sw();
        vq.sort();
        t2 = vq.elapsed();
        same = "true"; //compare(&pq,&vq) ? "true" : "false";
        std::cout << t2 << "\t" << same;
        std::cout << std::endl;
    }
}



int main() {
    tt.stepsz = 200000;
    tt.t = 0;
    int tests = 20;

    //int factor = 100000;
    std::cout.precision(8);
    inordertest(tests);
    reversetest(tests);
    randomtest(tests);
}