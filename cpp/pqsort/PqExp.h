//
// Created by ivan_ on 21-Sep-17.
//

#ifndef APPLIED_ALGORITHMS_CPP_PQEXP_H
#define APPLIED_ALGORITHMS_CPP_PQEXP_H

#include <queue>
#include <vector>
#include <algorithm>
#include <random>
#include <iostream>


using std::priority_queue;
using std::less;
using std::greater;
using std::vector;
#include <chrono>
#include <ctime>

class BaseExp {
     std::chrono::time_point<std::chrono::system_clock> start;
public:
    BaseExp() {
        start = std::chrono::system_clock::now();
    }
    virtual void insert(int i){};

    virtual void reset_sw() {
        start = std::chrono::system_clock::now();
    }
    virtual double elapsed() {
        const std::chrono::time_point<std::chrono::system_clock> end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed = end-start;
        return elapsed.count();
    }
    // n # of random numbers
    virtual void random_input(int n, int seed) {
        int r;
        std::random_device rd;
        std::default_random_engine el(rd());
        el.seed(seed);
        std::uniform_int_distribution<int> uid(INT32_MIN, INT32_MAX);
        for (int i = 0; i < n; ++i) {
            r = uid(el);
            this->insert(r);
        }
    }

    //1 to limit
    virtual void inorder_input(int limit) {
        for (int i = 0; i <= limit; ++i) {
            this->insert(i);
        }
    }

    // limit to 1
    virtual void reversed_input(int limit) {
        for (int i = limit; i > 0; i--) {
            this->insert(i);
        }
    }
};

class PqExp : public BaseExp {
    priority_queue<int, vector<int>, greater<int>> pq;

    void insert(int i) {
        pq.push(i);
    }

public:
    priority_queue<int, vector<int>, greater<int>> *getQueue() {
        return &pq;
    };
    void print() {
        //temp var to retain queue
        priority_queue<int, vector<int>, greater<int>> npq;

        while (!pq.empty()) {
            std::cerr << pq.top() << " ";
            npq.push(pq.top());
            pq.pop();
        }
        //to retain queue post iteration
        delete(&pq);
        pq = npq;
    }
};

class VectorExp : public BaseExp {
    vector<int> V;

    void insert(int i) {
        V.push_back(i);
    }

public:
    vector<int> *getVector() {
        return &V;
    };
    void sort() {
        std::sort(V.begin(), V.end(), less<int>());
    }
    void print() {
        vector<int>::iterator it;
        for (it = V.begin(); it < V.end(); it++) {
            std::cerr << *it << " ";
        }
    }

};


#endif //APPLIED_ALGORITHMS_CPP_PQEXP_H
