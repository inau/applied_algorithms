#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

struct offset {
    int col;
    int row;
    int block_sz;

    offset q1() {
        return offset{col, row, block_sz/2};
    }
    offset q2() {
        return offset{col, row+block_sz/2, block_sz/2};
    }
    offset q3() {
        return offset{col+block_sz/2, row, block_sz/2};
    }
    offset q4() {
        return offset{col+block_sz/2, row+block_sz/2, block_sz/2};
    }

    void print() {
        cout << "{ col: " << col << ", row: " << row <<", sz " << block_sz << " }" << endl;
    }
};

class Matrix
{
    public:
        //access
        int get(int x, int y) {
            return this->content[ rm(x,y) ];
        }
        int dim() {
            return this->N;
        }
        void print() {
            for (int i = 0; i < this->N; ++i) {
                for (int j = 0; j < this->N; ++j) {
                    cout << this->get(i,j) << " ";
                }
            }
            cout << endl;
        }
        void print(offset o) {
            for (int i = 0; i < o.block_sz; ++i) {
                for (int j = 0; j < o.block_sz; ++j) {
                    cout << this->get(o.col+i,o.row+j) << " ";
                }
            }
            cout << endl;
        }

        //mutaters
        void overwriteSection(offset off, Matrix src) {
            for (int i = 0; i < off.block_sz; i++) {
                for (int j = 0; j < off.block_sz; j++) {
                    this->set(off.col+j,off.row+i, src.get(j,i) );
                }
            }
        }

        void incrementBy(int x, int y, int v) {
            this->content[ rm(x,y) ] += v;
        }

        void set(int x, int y, int v) {
            this->content[ rm(x,y) ] = v;
        }

        void mul(Matrix other) {
            int* c = new int[N*N];
            for (int i = 0; i < other.dim(); i++) {
                for (int j = 0; j < other.dim(); j++) {
                    c[rm(i,j)] = 0;
                    for (int k = 0; k < other.dim(); k++) {
                        c[rm(i,j)] += this->get(i,k) * other.get(k,j);
                    }
                }
            }
            this->content = c;
        }

        void add(Matrix other) {
            for (int i = 0; i < other.dim(); i++) {
                for (int j = 0; j < other.dim(); j++) {
                    this->incrementBy( j,i, other.get(j,i) );
                }
            }
        }

        //constructor
        Matrix(int N) {
            this->N = N;
            this->content = new int[N*N];
        }

protected:
        int rm(int x, int y){
            return x + y * N;
        };
        int* content;
        int N;

        int* clone()
        {
            int size = this->N*N;
            int* clone = new int[size];
            memcpy(clone, this->content, size);
            return clone;
        }
};

Matrix initMatrix(istream *fs, int n) {
    Matrix m = Matrix(n);
    string line,is,js,xs;
    int i,j,x;

    while( getline(*fs, line) )  {
        if( line.front() == '#' ) continue;
        istringstream iss(line, istringstream::in);
        getline(iss, is, ',');
        getline(iss, js, ',');
        getline(iss, xs);

        i = stoi(is);
        j = stoi(js);
        x = stoi(xs);

        m.set( i,j,x );
    }

    return m;
}

void printStep(Matrix q, Matrix z) {
    cout << "-----" << endl;
    q.print();
    z.print();
    cout << "-----" << endl;
}

Matrix rec(Matrix A, offset aoff, Matrix B, offset boff) {
    Matrix c = Matrix(aoff.block_sz);
    if(aoff.block_sz > 1) {
        Matrix q0 = rec(A, aoff.q1(), B, boff.q1()), z0 = rec(A, aoff.q2(), B, boff.q3());
        printStep(q0,z0);
        q0.add(z0);
        c.overwriteSection(aoff.q1(), q0);

        Matrix q1 = rec(A, aoff.q2(), B, boff.q2()), z1 = rec(A, aoff.q2(), B, boff.q4());
        printStep(q1,z1);
        q1.add( z1 );
        c.overwriteSection(aoff.q2(), q1);

        Matrix q2 = rec(A, aoff.q3(), B, boff.q1()), z2 = rec(A, aoff.q4(), B, boff.q3());
        printStep(q2,z2);
        q2.add( z2 );
        c.overwriteSection(aoff.q3(), q2);

        Matrix q3 = rec(A, aoff.q3(), B, boff.q2()), z3 = rec(A, aoff.q4(), B, boff.q4());
        printStep(q3,z3);
        q3.add( z3 );
        c.overwriteSection(aoff.q4(), q3);
    }
    else {
        c.set(0,0, A.get(aoff.col, aoff.row) * B.get(boff.col, boff.row) );
    }
    return c;
}

int main(int argc, char** argv) {
    if(argc < 4) {
        cout << "not enough args, expects: N [fileA] [fileB]" << endl;
        exit(2);
    }

    int N = atoi(argv[1]);
    char* f1 = argv[2];
    char* f2 = argv[3];

    std::fstream ina, inb;
    ina.open(f1);
    if( ina.fail() ) {
        cout << "filestream failed for file " << f1 << endl;
        exit(2);
    }
    inb.open(f2);
    if( inb.fail() ) {
        cout << "filestream failed for file " << f2 << endl;
        exit(2);
    }

    Matrix A = initMatrix(&ina, N);
    Matrix B = initMatrix(&inb, N);

    A.print();
    B.print();

    Matrix C = rec(A, offset{0,0,N}, B, offset{0,0,N});

    C.print();
}