import java.util.Scanner;

public class Matrix {

    abstract static class MatrixFactory {
        public abstract IMatrix newMatrix(int dimension);

        public IMatrix init(Scanner s, int dimension) {
            IMatrix m = newMatrix(dimension);

            //clear comment line
            s.nextLine();
            while (s.hasNextLine() ) {
                String[] ints = s.nextLine().split(",");
                m.set(Integer.parseInt(ints[0]), Integer.parseInt(ints[1]), Integer.parseInt(ints[2]) );
            }

            return m;
        }
        public IMatrix initTransposed(Scanner s, int dimension) {
            IMatrix m = newMatrix(dimension);

            //clear comment line
            s.nextLine();
            while (s.hasNextLine() ) {
                String[] ints = s.nextLine().split(",");
                //transposition => x,y -> y,x
                m.set(Integer.parseInt(ints[1]), Integer.parseInt(ints[0]), Integer.parseInt(ints[2]) );
            }

            return m;
        }
    }

    interface IMatrix {
        public int get(int x, int y);
        public void set(int x, int y, int v);
        public void incrementBy(int x, int y, int v);
        public int dim();

        public IMatrix overwrite(int x, int y, int block, IMatrix src);

        public IMatrix mul(IMatrix other);
        public IMatrix add(IMatrix other);
    }

    static class M1D implements IMatrix {
        private int rowMajorAccess(int x, int y, int row_length) {
            return x + y * row_length;
        }

        final int N;
        final int[] content;

        public M1D(int N) {
            this.N = N;
            content = new int[N*N];
        }

        @Override
        public int get(int x, int y) {
            return content[ rowMajorAccess(x,y, N) ];
        }

        @Override
        public void set(int x, int y, int v) {
            content[ rowMajorAccess(x,y,N) ] = v;
        }

        @Override
        public void incrementBy(int x, int y, int v) {
            content[ rowMajorAccess(x,y,N) ] += v;
        }

        @Override
        public int dim() {
            return N;
        }

        @Override
        public IMatrix overwrite(int x, int y, int block, IMatrix src) {
            for (int i = 0; i < block; i++) {
                for (int j = 0; j < block; j++) {
                    this.set(x + i, y + j, src.get(i, j) );
                }
            }
            return this;
        }

        @Override
        public IMatrix mul(IMatrix other) {
            IMatrix C = new M1D(this.N);
            for(int i = 0; i < other.dim() ; i++)
                for(int j = 0; j < other.dim() ; j++){
                    for(int k = 0; k < other.dim() ; k++)
                        C.incrementBy(i,j, this.get(i,k) * other.get(k,j) );
                }
            return C;
        }

        @Override
        public IMatrix add(IMatrix other) {
            for(int i = 0; i < other.dim() ; i++)
                for(int j = 0; j < other.dim() ; j++)
                    this.incrementBy( i,j, other.get(i,j) );
            return this;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < N; i++)
                for (int j = 0; j < N; j++)
                    System.out.print( get(i,j) + " " );
            return sb.toString();
        }
    }

    static class M2D implements IMatrix {
        final int[][] content;

        public M2D(int N) {
            content = new int[N][N];
        }

        @Override
        public int get(int x, int y) {
            return content[x][y];
        }

        @Override
        public void set(int x, int y, int v) {
            content[x][y] = v;
        }

        @Override
        public void incrementBy(int x, int y, int v) {
            content[x][y] += v;
        }

        @Override
        public int dim() {
            return content.length;
        }

        @Override
        public IMatrix overwrite(int x, int y, int block, IMatrix src) {
            return this;
        }

        @Override
        public IMatrix mul(IMatrix other) {
    return this;
        }

        @Override
        public IMatrix add(IMatrix other) {
            return this;
        }

    }

    public static MatrixFactory Get1DFactory() {
        return new MatrixFactory() {
            @Override
            public IMatrix newMatrix(int dimension) {
                return new M1D(dimension);
            }

        };
    }

    public static MatrixFactory Get2DFactory() {
        return new MatrixFactory() {
            @Override
            public IMatrix newMatrix(int dimension) {
                return new M2D(dimension);
            }
        };
    }

}
