import org.omg.CORBA.MARSHAL;

import javax.xml.ws.soap.MTOM;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;

/**
 * Created by Ivan on 28-Aug-17.
 */
public class mxm {

    interface Matrix {
        public int get(int x, int y);
        public void set(int x, int y, int v);
        public void incrementBy(int x, int y, int v);
        public int dim();
    }

    static class M1D implements Matrix {
        private int rowMajorAccess(int x, int y, int row_length) {
            return x + y * row_length;
        }

        final int N;
        final int[] content;

        public M1D(int N) {
            this.N = N;
            content = new int[N*N];
        }

        @Override
        public int get(int x, int y) {
            return content[ rowMajorAccess(x,y, N) ];
        }

        @Override
        public void set(int x, int y, int v) {
            content[ rowMajorAccess(x,y,N) ] = v;
        }

        @Override
        public void incrementBy(int x, int y, int v) {
            content[ rowMajorAccess(x,y,N) ] += v;
        }

        @Override
        public int dim() {
            return N;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < N; i++)
                for (int j = 0; j < N; j++)
                    System.out.print( get(i,j) + " " );
            return sb.toString();
        }
    }

    static class M2D implements Matrix {
        private int rowMajorAccess(int x, int y, int row_length) {
            return x + y * row_length;
        }
        final int[][] content;

        public M2D(int N) {
            content = new int[N][N];
        }

        @Override
        public int get(int x, int y) {
            return content[x][y];
        }

        @Override
        public void set(int x, int y, int v) {
            content[x][y] = v;
        }

        @Override
        public void incrementBy(int x, int y, int v) {
            content[x][y] += v;
        }

        @Override
        public int dim() {
            return content.length;
        }
    }

    private static Matrix initMatrix(Scanner s, int N) {
        Matrix m = new M1D(N);

        //clear comment line
        s.nextLine();
        while (s.hasNextLine() ) {
            String[] ints = s.nextLine().split(",");
            m.set(Integer.parseInt(ints[0]), Integer.parseInt(ints[1]), Integer.parseInt(ints[2]) );
        }

        return m;
    }

    private static Matrix initMatrix2D(Scanner s, int N) {
        Matrix m = new M2D(N);
        //clear comment line
        s.nextLine();

        while (s.hasNextLine() ) {
            String[] ints = s.nextLine().split(",");
            m.set(Integer.parseInt(ints[0]), Integer.parseInt(ints[1]), Integer.parseInt(ints[2]) );
        }

        return m;
    }

    private static void printMatrix(int[][] mx, int N) {
        printResultMatrixOffset(0,0,mx,N, true);
    }
    private static void printResultMatrix(int[][] mx, int N) {
        printResultMatrixOffset(0,0,mx,N, false);
    }
    private static void printResultMatrixOffset(int x, int y, int[][] mx, int N, boolean line_end) {
        for(int i = x; i < N; i++) {
            for (int j = y; j < N; j++) {
                System.out.print(mx[i][j] + " ");
            }
            if(line_end) System.out.println("");
        }
    }

    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        if(args.length < 3) throw new IllegalArgumentException("Expects [int] [file_mat_a] [file_mat_b]");
        FileInputStream
                fa = new FileInputStream(args[1]),
                fb = new FileInputStream(args[2]);

        int n = Integer.parseInt(args[0]);

        //for (int i = 2; i < 32; i= i*2) {
        //    testSplitAndCombine(i);
        //}

        Scanner in_a = new Scanner(fa);
        Scanner in_b = new Scanner(fb);

        Matrix A, B, C;
        A = initMatrix(in_a, n);
        B = initMatrix(in_b, n);

        //C = rec_cpy(A,B);
        C = naive(A,B,n);
        System.out.println( C.toString() );
        //printResultMatrix(C, n);
    }

    /**
     * Naive implementation of MxM of matrices A and B
     * @param A
     * @param B
     * @param n
     * @return matrix C
     */
    private static Matrix naive(Matrix A, Matrix B, int n) {
        Matrix C = new M1D(n);
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                for (int k = 0; k < n; k++){
                    C.incrementBy(i, j, A.get(i,k) * B.get(k,j) );
                }
        return C;
    }

    //assumes equal lengths (square matrices)
    private static Matrix mul2D(Matrix A, Matrix B) {
        Matrix C = new M1D(A.dim());
        for (int i = 0; i < A.dim(); i++)
            for (int j = 0; j < A.dim(); j++)
                for (int k = 0; k < A.dim(); k++)
                    C.set( i,j, A.get(i,k) * B.get(k,j) );
        return C;
    }

    //assumes equal lengths (square matrices)
    private static Matrix add2D(Matrix A, Matrix B) {
        Matrix C = new M1D(A.dim());
        for (int i = 0; i < A.dim(); i++)
            for (int j = 0; j < A.dim(); j++)
                C.set( i, j, A.get(i, j) + B.get(i, j) );
        return C;
    }

    private static Matrix subset(Matrix src, int x, int y, int block_size) {
        Matrix res = new M1D(block_size);
        for(int i = 0; i < block_size; i++)
            for(int j = 0; j < block_size; j++)
                res.set( i,j, src.get(x+i, y+j) );
        return res;
    }

    private static Matrix[] partition(Matrix A) {
        int block = A.dim()/2;
        Matrix[] res = new Matrix[4];

        res[0]  = subset(A, 0, 0, block);
        res[1]  = subset(A, 0, block, block);
        res[2]  = subset(A, block, 0, block);
        res[3]  = subset(A, block, block, block);

        return res;
    }

    //insert source matrix of block size starting at (x,y) and ending at (x+block,y+block) into target matrix
    private static void insert(final int x, final int y, final int block, Matrix src, Matrix tar) {
        for (int i = 0; i < block; i++) {
            for (int j = 0; j < block; j++) {
                tar.set(x + i, y + j, src.get(i, j) );
            }
        }
    }

    //merges 4 matrices of NxN into a 2Nx2N matrix
    private static Matrix mergeSubsets(Matrix A0, Matrix A1, Matrix A2, Matrix A3) {
        Matrix res = new M1D(A0.dim()*2);

        insert(0,0, A0.dim(), A0, res);
        insert(0,A1.dim(), A1.dim(), A1, res);
        insert(A2.dim(), 0, A2.dim(), A2, res);
        insert(A3.dim(), A3.dim(), A3.dim(), A3, res);

        return res;
    }

    private static void printParts(int[][][] parts) {
        for (int i = 0; i < parts.length; i++){
            printResultMatrix(parts[i], parts[i].length);
            System.out.println("");
        }
    }

    private static Matrix rec_cpy_threaded(Matrix A, Matrix B) throws ExecutionException, InterruptedException {
        ExecutorService exec = Executors.newCachedThreadPool();
        final Matrix[] apart = partition(A);
        final Matrix[] bpart = partition(B);

        List<Callable<Matrix>> workloads = Arrays.asList(
                () -> add2D( rec_cpy(apart[0], bpart[0]), rec_cpy(apart[1], bpart[2]) ),
                () -> add2D( rec_cpy(apart[0], bpart[1]), rec_cpy(apart[1], bpart[3]) ),
                () -> add2D( rec_cpy(apart[2], bpart[0]), rec_cpy(apart[3], bpart[2]) ),
                () -> add2D( rec_cpy(apart[2], bpart[1]), rec_cpy(apart[3], bpart[3]) )
        );

        Iterator<Matrix> it =
                exec.invokeAll(workloads)
                .parallelStream()
                .map(r -> {
                    try {
                        return r.get();
                    } catch (Exception e) {
                        throw new IllegalStateException();
                    }
                }).iterator();
        exec.shutdown();

        return mergeSubsets(
                it.next(),it.next(),it.next(),it.next()
                );
    }

    private static Matrix rec_cpy(Matrix A, Matrix B) {
        if(A.dim() > 1) {
            Matrix[] apart = partition(A);
            Matrix[] bpart = partition(B);

            return mergeSubsets(
                    add2D( rec_cpy(apart[0], bpart[0]), rec_cpy(apart[1], bpart[2]) ),
                    add2D( rec_cpy(apart[0], bpart[1]), rec_cpy(apart[1], bpart[3]) ),
                    add2D( rec_cpy(apart[2], bpart[0]), rec_cpy(apart[3], bpart[2]) ),
                    add2D( rec_cpy(apart[2], bpart[1]), rec_cpy(apart[3], bpart[3]) )
            );

        }
        else {
            Matrix m = new M1D(A.dim());
            m.set( 0,0, A.get(0,0) * B.get(0,0) );
            return m;
        }
    }


    public static void testSplitAndCombine(int size) {
        Matrix og = new M1D(size);
        for (int i = 0; i < size; i++)
            for (int j = 0; j < size; j++)
                og.set(i, j, j);

        Matrix[] parts = partition(og);

        Matrix recombined = mergeSubsets(parts[0],parts[1],parts[2],parts[3]);


        System.out.println("Original\n" + og.toString() );
        System.out.println("Recombined\n" +recombined.toString() );
        System.out.println("");

        assert og.equals(recombined);

    }

}
