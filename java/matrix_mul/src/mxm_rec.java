import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class mxm_rec {

    public static class SubsetIterator implements Matrix.IMatrix {
        final Matrix.IMatrix src;
        final int x, y, block_size;

        public SubsetIterator(final Matrix.IMatrix src, final int x, final int y, final int block_size) {
            this.src = src;
            this.x = x;
            this.y = y;
            this.block_size = block_size;
        }

        @Override
        public int get(int x, int y) {
            return src.get( this.x+x, this.y+y );
        }

        @Override
        public void set(int x, int y, int v) {
            throw new RuntimeException("Not supported by this iterator...");
        }

        @Override
        public void incrementBy(int x, int y, int v) {
            throw new RuntimeException("Not supported by this iterator...");
        }

        @Override
        public int dim() {
            return block_size;
        }

        @Override
        public Matrix.IMatrix overwrite(int x, int y, int block, Matrix.IMatrix src) {
            throw new RuntimeException("Not supported by this iterator...");
        }

        @Override
        public Matrix.IMatrix mul(Matrix.IMatrix other) {
            throw new RuntimeException("Not supported by this iterator...");
        }

        @Override
        public Matrix.IMatrix add(Matrix.IMatrix other) throws NotImplementedException {
            throw new RuntimeException("Not supported by this iterator...");
        }

    }

    public static void main(String[] args) throws FileNotFoundException {
        Matrix.MatrixFactory mf = Matrix.Get1DFactory();
        if(args.length < 3) throw new IllegalArgumentException("Expects [int] [file_mat_a] [file_mat_b]");
        FileInputStream
                fa = new FileInputStream(args[1]),
                fb = new FileInputStream(args[2]);

        int n = Integer.parseInt(args[0]);

        Scanner in_a = new Scanner(fa);
        Scanner in_b = new Scanner(fb);

        Matrix.IMatrix  A = mf.init(in_a, n),
                        B = mf.init(in_b, n),
                //C = rec_cpy(A, 0,0,B,0,0,n,mf);
                C = rec_cpy(A,B,mf);

        System.out.println( C.toString() );
    }

    public static SubsetIterator[] partitionMatrix(Matrix.IMatrix m) {
        int block = m.dim() / 2;
        return new SubsetIterator[] {
                new SubsetIterator(m, 0,0, block),
                new SubsetIterator(m, 0,block, block),
                new SubsetIterator(m, block,0, block),
                new SubsetIterator(m, block,block, block)
        };
    }

    public static void stitchMatrix(Matrix.IMatrix src, Matrix.IMatrix Q0, Matrix.IMatrix Q1, Matrix.IMatrix Q2, Matrix.IMatrix Q3) {
        src.overwrite(0,0, Q0.dim(), Q0);
        src.overwrite(0,Q1.dim(), Q1.dim(), Q1);
        src.overwrite(Q2.dim(), 0, Q2.dim(), Q2);
        src.overwrite(Q3.dim(), Q3.dim(), Q3.dim(), Q3);
    }

    public static void stitchMatrix(Matrix.IMatrix src, Matrix.IMatrix Q0, Matrix.IMatrix Q1, Matrix.IMatrix Q2, Matrix.IMatrix Q3, int dim) {
        src.overwrite(0,0, dim, Q0);
        src.overwrite(0,dim, dim, Q1);
        src.overwrite(dim, 0, dim, Q2);
        src.overwrite(dim, dim, dim, Q3);
    }

    private static Matrix.IMatrix rec_cpy(Matrix.IMatrix A, Matrix.IMatrix B, Matrix.MatrixFactory mf) {
        final Matrix.IMatrix C = mf.newMatrix(A.dim());
        if(A.dim() > 1) {
            SubsetIterator[] apart = partitionMatrix(A);
            SubsetIterator[] bpart = partitionMatrix(B);

            stitchMatrix(C ,
                    rec_cpy(apart[0], bpart[0], mf).add(rec_cpy(apart[1], bpart[2], mf) ),
                    rec_cpy(apart[0], bpart[1], mf).add(rec_cpy(apart[1], bpart[3], mf) ),
                    rec_cpy(apart[2], bpart[0], mf).add( rec_cpy(apart[3], bpart[2], mf) ),
                    rec_cpy(apart[2], bpart[1], mf).add( rec_cpy(apart[3], bpart[3], mf) )
            );
            return C;

        }
        else {
            C.set( 0,0, A.get(0,0) * B.get(0,0) );
            return C;
        }
    }

    //attempt to use offsets rather than 'subsets'
    static int ni(int oldi, int q, int d) {
        switch (q) {
            case 0:
                return oldi+0;
            case 1:
                return oldi+d;
            case 2:
                return oldi+0;
            case 3:
                return oldi+d;

        }
        return oldi;
    }
    static int nj(int oldj, int q, int d) {
        switch (q) {
            case 0:
                return oldj+0;
            case 1:
                return oldj+0;
            case 2:
                return oldj+d;
            case 3:
                return oldj+d;
        }
        return oldj;
    }
    private static Matrix.IMatrix rec_cpy(final Matrix.IMatrix A, int ai, int aj, final Matrix.IMatrix B, int bi, int bj, int dim, Matrix.MatrixFactory mf) {
        System.out.printf("A(%d, %d) * B(%d, %d) (D=%d)\n", ai,aj,bi,bj,dim);
        Matrix.IMatrix C = mf.newMatrix(dim);
        if(dim > 1) {
            int block = dim/2;

            stitchMatrix(C,
                    rec_cpy(A, ni(ai,0,block), nj(aj,0, block),
                            B, ni(bi,0,block), nj(bj,0, block), block, mf)
                            .add(rec_cpy(   A, ni(ai,1, block), nj(aj,1, block),
                                            B, ni(bi,2, block), nj(bj, 2, block), block, mf) ),
                    rec_cpy(A, ni(ai,0,block), nj(aj,0, block),
                            B, ni(bi,1,block), nj(bj,1, block), block, mf)
                            .add(rec_cpy(   A, ni(ai,1, block), nj(aj,1, block),
                                            B, ni(bi,3, block), nj(bj, 3, block), block, mf) ),
                    rec_cpy(A, ni(ai,2,block), nj(aj,2, block),
                            B, ni(bi,0,block), nj(bj,0, block), block, mf)
                            .add(rec_cpy(   A, ni(ai,3, block), nj(aj,3, block),
                                            B, ni(bi,2, block), nj(bj, 2, block), block, mf) ),
                    rec_cpy(A, ni(ai,2,block), nj(aj,2, block),
                            B, ni(bi,1,block), nj(bj,1, block), block, mf)
                            .add(rec_cpy(   A, ni(ai,3, block), nj(aj,3, block),
                                            B, ni(bi,3, block), nj(bj, 3, block), block, mf) ),
                    block
            );
            return C;

        }
        else {
            C.set( 0,0, A.get(ai,aj) * B.get(bi,bj) );
            return C;
        }
    }

}
