import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Maintain a dynamic set under insertion, reporting the approximate number of distinct items inserted using a HyperLogLog counter.
 * @author Rasmus Pagh
 * @version 2016.11.21
 */

public class ApproxSet<T extends Object> {

	Map<Integer, T> objects = new HashMap<>();
	private static int logm = 10;
	private static int m = 1<<logm;
	private byte[] M = new byte[m];

	public void add(T x) {
		int xh = x.hashCode();
		if (xh!=0) {
			int i = LinearHash.f(xh,logm);
			byte val = (byte)ExpHash.hash(xh);
			objects.put(i, x);
			if (val>M[i]) M[i]=val;
		}
	}

	public void addSet(ApproxSet<T> other) {
		for( T o : other.objects.values() ) {
			this.add(o);
		}
	}
	
	public int sizeEstimate() {
		double wsum = 0;
		int zerosum = 0;
		for (int j=0; j<m; j++) {
			wsum += Math.pow(2.0,-M[j]);
			if (M[j]==0) zerosum++;
		}
		double Z = 1/wsum;
		double estimate = m*m*Z*0.7213/(1 + 1.079/m);
		if ((estimate < 2.5 * m) && (zerosum>0))
			estimate = m * Math.log((double)m/zerosum);
		return (int)estimate;
	}

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		ApproxSet[] sets = new ApproxSet[] {
			new ApproxSet(),new ApproxSet()
		};
		String l; String[] ll; int seti = 0;
		while( s.hasNextLine()) {
			l = s.nextLine();
			ll = l.split(" ");
			if(ll.length > 0) {
				for( String ls : ll ) {
					sets[seti].add( Integer.parseInt(ls) );
				}
			}
			seti++;
		}

		int szA = sets[0].sizeEstimate();
		int szB = sets[1].sizeEstimate();
		sets[0].addSet( sets[1] );
		int szAB = sets[0].sizeEstimate();

		double ratio = szA / szAB;
		System.err.printf("r %f, A: %d, B: %d, AuB: %d", ratio, szA, szB, szAB);
		System.out.printf("almost %s\n", (ratio > 0.8) ? "same" : "disjoint");
	}

}
